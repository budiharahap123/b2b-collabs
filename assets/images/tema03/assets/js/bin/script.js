(function ($) {	

	/!* DROP DOWN SELECTOR */
	$.fn.listSelector = function(){

		this.each(function(){

			var obj = $(this);

			$('.option' , obj).on('click' , function(){

				var value = $(this).attr('data-value');
				var text = $(this).text();
				
				$('.value_set' , obj).val(value);
				$('.cat_display' , obj).html(text);


			});

		});

	};

	/!* PRODUCT GALLERY */
	$.fn.prodGallery = function(data){

		this.each(function(){
			var prodinfo = data.productinfo;

			var obj = $(this);
			var change_trigger = $('.trigger' , obj);
			var preloader = '<div class="preloader"></div>';
			var target = $('.imgchange_container' , obj);

			//untuk merubah product description
			function changeProdDesc(e){
				$('.product-info-list' , obj).hide();
				$('.product-info-list.'+e , obj).show();
			}

			//untuk merubah product thumbnail state selected
			function changeThumbState(e){

				change_trigger.removeClass('selected');
				e.addClass('selected');
			}

			//untuk merubah gambar besar
			function changeImg(e){

				target.prepend(preloader);

				$('img' , target).attr('src', e) .load(function() {	
					$('.preloader' , target).remove();
				});

			}

			change_trigger.on('click' , function(){
				var this_thumbnail = $(this);
				var target_gambar = this_thumbnail.attr('data-target');

				changeImg(target_gambar);
				changeThumbState(this_thumbnail);

				if(prodinfo == true){
					var prodinfos = this_thumbnail.attr('data-prodinfo');
					changeProdDesc(prodinfos);
				}

			});


		});

	};

})(jQuery);