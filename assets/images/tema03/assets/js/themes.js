$(document).on('ready', function(){

/* Smooth Scroll */
  if (window.addEventListener) window.addEventListener('DOMMouseScroll', wheel, false);
  window.onmousewheel = document.onmousewheel = wheel;
  function wheel(event) {
    var delta = 0;
    if (event.wheelDelta) delta = event.wheelDelta / 120;
    else if (event.detail) delta = -event.detail / 3;
    handle(delta);
    if (event.preventDefault) event.preventDefault();
    event.returnValue = false;
  }
  var goUp = true;
  var end = null;
  var interval = null;
  function handle(delta) {
    var animationInterval = 10; //lower is faster
    var scrollSpeed = 10; //lower is faster
    if (end == null) {
      end = $(window).scrollTop();
    }
    end -= 20 * delta;
    goUp = delta > 0;
    if (interval == null) {
      interval = setInterval(function () {
        var scrollTop = $(window).scrollTop();
        var step = Math.round((end - scrollTop) / scrollSpeed);
        if (scrollTop <= 0 || 
        scrollTop >= $(window).prop("scrollHeight") - $(window).height() ||
        goUp && step > -1 || 
        !goUp && step < 1 ) {
          clearInterval(interval);
          interval = null;
          end = null;
        }
        $(window).scrollTop(scrollTop + step );
      }, animationInterval);
    }
  }

/* Product Animation */
$(".fade_in").each(function(e){
   var $this = $(this);
   setTimeout(function () {
    $this.animate({
    opacity: 1
    }, {
     duration: 600,
     queue: false
    });
    $this.animate({
     duration: 1200,
     specialEasing: {
      "margin-right": "easeOutCirc"
     },
     queue: false
    });
   }, 600 * e + 1);
  });


/* Change To Semirow*/
  $('.ico-list-grid').addClass('curr');
  $('.list-semirow').click(function(e){
    $('.theme-list-product').addClass('semi-list_product');
    $('.ico-list-semirow').addClass('curr');
    $('.ico-list-grid').removeClass('curr');
  });

  $('.list-grid').click(function(e){
    $('.theme-list-product').removeClass('semi-list_product');
    $('.ico-list-semirow').removeClass('curr');
    $('.ico-list-grid').addClass('curr');
  });

/* Pop Up Contact */

  $('#open-popup').click(function(e){
    $('#popup').css('display','block');
    $('#bg').css('display','block');
    $('#bg').animate({'opacity':'0.8'}, 300, 'linear');
    $('#popup').animate({'opacity':'1.00'},300,'linear');
  });
    
  $('#close-popup').click(function(e){
    $('#popup, #bg').animate({'opacity':'0'},300,'linear', function(){
      $('#popup, #bg').css('display','none');
    });
  });

  $('#bg').click(function(e){
    $('#popup, #bg').animate({'opacity':'0'},300,'linear', function(){
      $('#popup, #bg').css('display','none');
    });
  });
  $(document).keyup(function(e) {
    if (e.keyCode == 27) { // Esc
      $('#popup, #bg').animate({'opacity':'0'},300,'linear', function(){
        $('#popup, #bg').css('display','none');
      });
    }
  });
  
    /* Slick */
$('.multiple-items').slick({
  dots: true,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 3000,
  speed: 3000,
  slidesToShow: 2,
  slidesToScroll: 2
});
});


$(window).scroll(function() {
 var toolfix = $(document).height() - 1500;    
 if ($(this).scrollTop() > 650 && $(this).scrollTop() < toolfix ){  
  $('.full-container').addClass("cp-fixedtoolbar");
 }else{
  $('.full-container').removeClass("cp-fixedtoolbar");
 }
});

// Script Read More
$(document).ready(function() {
      
  $('.showmore_one').showMore({
    speedDown: 300,
          speedUp: 300,
          height: '101px',
          showText: 'Show more',
          hideText: 'Show less'
  });
        
});