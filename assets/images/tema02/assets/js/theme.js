//Mulai list-grid dan smooth scroll
function init(){
	showItem();

	$('.list-semirow').click(function(e){
		$('.product').addClass('list-content');
		$('.product').find('.item').addClass('con');		
	});

	$('.list-grid').click(function(e){
		$('.product').removeClass('list-content');
		$('.product').find('.item').removeClass('con');
	});
	if (window.addEventListener) window.addEventListener('DOMMouseScroll', wheel, false);
}
//Selesai list-grid dan smooth scroll
function showItem(){
	$(".item").each(function(e){
		var $this = $(this);
		setTimeout(function () {
			$this.animate({
			opacity: 1
			}, {
				duration: 1200,
				queue: false
			});
			$this.animate({
				"margin-top": "0px"
			}, {
				duration: 1200,
				specialEasing: {
					"margin-top": "easeOutCirc"
				},
				queue: false
			});
		}, 600 * e + 1);
	});
}
$(window).on('load',init);
$(window).scroll(function() {
	var footup = $(document).height() - 1200;
	var toolfix = $(document).height() - 880;
	if ($(this).scrollTop() > 400 ){  
		$('.full-container').addClass("cp-fixed");
	}else{
		$('.full-container').removeClass("cp-fixed");
	}

	if ($(this).scrollTop() > footup){  
		$('.full-container').addClass("cp-footermargin");
	}else{
		$('.full-container').removeClass("cp-footermargin");
	}

	if ($(this).scrollTop() > 500 && $(this).scrollTop() < toolfix ){  
		$('.full-container').addClass("cp-fixedtoolbar");
	}else{
		$('.full-container').removeClass("cp-fixedtoolbar");
	}
});
$(document).on('ready', function(){
    $modal = $('.modal-frame');
    $overlay = $('.modal-overlay');

    /* Need this to clear out the keyframe classes so they dont clash with each other between ener/leave. Cheers. */
    $modal.bind('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(e){
      if($modal.hasClass('state-leave')) {
        $modal.removeClass('state-leave');
      }
    });

    $('.close').on('click', function(){
      $overlay.removeClass('state-show');
      $modal.removeClass('state-appear').addClass('state-leave');
    });

    $('.open').on('click', function(){
      console.log('a');
      $overlay.addClass('state-show');
      $modal.removeClass('state-leave').addClass('state-appear');
    });

  });


// Script Read More
$(document).ready(function() {
     	
	$('.showmore_one').showMore({
		speedDown: 300,
	        speedUp: 300,
	        height: '100px',
	        showText: 'Show more',
	        hideText: 'Show less'
	});
	      
});


//Mulai Poup
$('.send-btn').click(function(e){
			$('#popup').css('display','block');
			$('#bg').css('display','block');
			$('#bg').animate({'opacity':'0.8'}, 300, 'linear');
			$('#popup').animate({'opacity':'1.00'},300,'linear');
		});
		
		$('#close-popup').click(function(e){
			$('#popup, #bg').animate({'opacity':'0'},300,'linear', function(){$('#popup, #bg').css('display','none');
			});
		});
$('#bg').click(function(e){
		$('#popup, #bg').animate({'opacity':'0'},300,'linear', function(){$('#popup, #bg').css('display','none');
		});
	});
	$(document).keyup(function(e) {
	    if (e.keyCode == 27) { // Esc
	        $('#popup, #bg').animate({'opacity':'0'},300,'linear', function(){$('#popup, #bg').css('display','none');
			});
	    }
	});
// Selesai Popup