	// STICKY HEADER
	$(window).scroll(function() {
		var curr_view = $('body').attr('class');
		var footup = $(document).height() - 1000;
		var toolfix = $(document).height() - 1500;

		if ($(this).scrollTop() > 46){  
			$('.totop').addClass("showme");
			$('body').addClass("fixed-margin");
		}else{
			$('.totop').removeClass("showme");
			$('body').removeClass("fixed-margin");
		}
		if ($(this).scrollTop() > footup){  
			$('body').addClass("footer-margin");
		}else{
			$('body').removeClass("footer-margin");
		}
		
		// TOOLBAR 
		if(curr_view.search('listing') != -1 && curr_view.search('profile-store') == -1) {
			if ($(this).scrollTop() > 100 && $(this).scrollTop() < toolfix ){  
				$('body').addClass("fixed-toolbar");
			}else{
				$('body').removeClass("fixed-toolbar");
			}
			if ($(this).scrollTop() > toolfix ){  
				$('body').addClass("footer-toolbar");
			}else{
				$('body').removeClass("footer-toolbar");
			}
		}

		//PROFILE STORE
		if(curr_view.search('profile-store') != -1) {
			if ($(this).scrollTop() > 100){  
				$('body').addClass("fixed-profile");
			}else{
				$('body').removeClass("fixed-profile");
			}
			if ($(this).scrollTop() > 270 && $(this).scrollTop() < toolfix ){  
				$('body').addClass("fixed-toolbar");
			}else{
				$('body').removeClass("fixed-toolbar");
			}
			if ($(this).scrollTop() > toolfix ){  
				$('body').addClass("footer-toolbar");
			}else{
				$('body').removeClass("footer-toolbar");
			}

		}

	});

$(function(){

	// QUICKVIEW 	
	function showPopUp(){
		$('body').addClass('quickview');
		$(document).keyup(function(e) {
		    if (e.keyCode == 27) { 
		    	hidePopUp();
		    	window.history.back();
		    }
		});
	}

	function hidePopUp(){
		$('body').removeClass('quickview');
	//	$(document).unbind("keyup", keyUpFunc);
		$(".overthetop").animate({ scrollTop: 0 }, "fast");
	}

	function showDetailContent(x){
		var target = x;

		showPopUp();

		$('#quickview-box').html(
			'<iframe class="quickview-iframe" src="'+target+'" id="quickview-iframe" scrolling="no"></iframe>'
		);


		$('#quickview-iframe').on('load', function() {
		    $('#quickview-iframe').contents().find("head")
		      .prepend($('<style type="text/css">.header, .breadcrump, .toolbar, .footer {display:none;} body{margin-top:-137px}</style>'));
		    $('#quickview-iframe').contents().find("head")
		      .append('<link href="assets/css/quickview.css" media="screen" rel="stylesheet" type="text/css">');
			$('#quickview-iframe').contents().find("#loadJS").append('$(".list-item  a").unbind("click");');
		});

		window.history.pushState("quickview", "page quickview test", target);
		
	}

	$('.list-item  a').click( function(e){
		e.preventDefault();
		var target = $(this).attr('href');
		showDetailContent(target);

	});

	$('.quickview-nav .close-btn , .overthetop .background').on('click' ,  function(){
		window.history.back();
		hidePopUp();
		
	});

	if (window.history && window.history.pushState) {
	    $(window).on('popstate', function(e) {
	    	var state = e.originalEvent.state;
		    if ( state === null ) { 
	        		hidePopUp();
			} else { 
			       	showPopUp();
			}
	    });

	}

});

// QUICKVIEW-IFRAME HEIGHT 
function iframeloaded() {
	$('#quickview-iframe').load(function() {
		this.style.height = this.contentWindow.document.body.scrollHeight + 'px';
	});
}

// LIST VIEW
$('#list-view ico').click( function() {
	var new_view = $(this).attr('rel');
	var curr_view = $('body').attr('class');
	if(curr_view.search('grid') != -1) { $('body').removeClass('grid'); }
	else if(curr_view.search('semirow') != -1) { $('body').removeClass('semirow'); }
	else if(curr_view.search('row') != -1) { $('body').removeClass('row'); }
	
	$('body').addClass(new_view);

});

// SLIDE THUMBNAIL DETAIL PAGE
	/!* PRODUCT PAGE GALLERY*/
	$.fn.prodGallerydetail = function(){

		this.each(function(){
			var obj = $(this);
			var thumb_trigger = $('.trigger' , this);
			var nav_trigger = $('.nav' ,this);
			var preloader = $('.preloader' , this);
			var target = $('.imgbox' , this);
			var state = 1;

			function changeImg(i , t){

				target.addClass('loading');

				$('.img' , target).attr('src', i) .load(function() {	target.removeClass('loading');	});

				thumb_trigger.removeClass('on');
				t.addClass('on');
				preloader.addClass('hide');

			}

			thumb_trigger.on('click' , function(){
				var posisi = (($(this).parent('.list').index())+1);
				var url_img = $(this).attr('rel');	
				var thumb_target = $(this);
				changeImg(url_img , thumb_target);		

				state = posisi;
			});


		});

	};

