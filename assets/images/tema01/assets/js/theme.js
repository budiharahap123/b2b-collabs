function init(){
	showItem();
	
	//toggle grid/list
	$('.list-semirow').click(function(e){
		$('.product-content').addClass('list-content');
		$('.product-content').find('.in-lg-4').addClass('in-lg-12');		
		$('.product-content').find('.product').addClass('con');		
	});

	$('.list-grid').click(function(e){
		$('.product-content').removeClass('list-content');
		$('.product-content').find('.in-lg-4').removeClass('in-lg-12');
		$('.product-content').find('.product').removeClass('con');
	});
	

	//lightbox	
	$('#btn-msg').click(function(e){
		$('#lightbox').css('display','block');
		$('#theme-overlay').css('display','block');
		$('#theme-overlay').animate({'opacity':'0.8'}, 300, 'linear');
		$('#lightbox').animate({'opacity':'1.00'},300,'linear');
	});
	
	$('#close-lightbox').click(function(e){
		$('#lightbox, #theme-overlay').animate({'opacity':'0'},300,'linear', function(){$('#lightbox, #theme-overlay').css('display','none');
		});
	});
	
	$('#theme-overlay').click(function(e){
		$('#lightbox, #theme-overlay').animate({'opacity':'0'},300,'linear', function(){$('#lightbox, #theme-overlay').css('display','none');
		});
	});
	
	$(document).keyup(function(e) {
	    if (e.keyCode == 27) { // Esc
	        $('#lightbox, #theme-overlay').animate({'opacity':'0'},300,'linear', function(){$('#lightbox, #theme-overlay').css('display','none');
			});
	    }
	});


	
	//toggle detail produk
	$('.toggle-detail').find('a').click(function(){
		var idx = $(this).index();
		$('.toggle-detail').find('a').removeClass('active');
		$(this).addClass('active');
		var tabLength = $('.cpdetail').find('section').size();
		
		for(var i = 0; i < tabLength; i++)
		{
			if(i != idx)
			{
				$('.cpdetail').find('section:eq('+i+')').css('display','none');
			}
			else
			{
				$('.cpdetail').find('section:eq('+i+')').css('display','block');
			}
		}
	});
}




//product effect
function showItem(){
	$(".product").each(function(e){
		var $this = $(this);
		setTimeout(function () {
			$this.animate({
			opacity: 1
			}, {
				duration: 1000,
				queue: false
			});
			$this.animate({
				"margin-top": "0px"
			}, {
				duration: 1000,
				specialEasing: {
					"margin-top": "easeOutCirc"
				},
				queue: false
			});
		}, 600 * e + 1);
	});
}
$(window).on('load',init);

//fixed sidebar/company info
$(window).scroll(function() {
	var footup = $(document).height() - 1600;
	if ($(this).scrollTop() > 700){  
		$('.full-container').addClass("cp-fixed");
	}else{
		$('.full-container').removeClass("cp-fixed");
	}
	
	if ($(this).scrollTop() > 1700){  
		$('.full-container').addClass("cp-footermargin");
	}else{
		$('.full-container').removeClass("cp-footermargin");
	}
});


// Script Read More
$(document).ready(function() {
     	
	$('.showmore_one').showMore({
		speedDown: 300,
	        speedUp: 300,
	        height: '100px',
	        showText: 'Show more',
	        hideText: 'Show less'
	});
	      
});