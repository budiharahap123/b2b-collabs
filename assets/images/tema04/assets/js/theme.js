function init(){
	showItem();
	// toggle item
	$('.list-semirow').click(function(e){

		$('.theme-item-content').addClass('theme-list-content');
		$('.theme-item-content').find('.theme-item').addClass('con');	
		$('.ico-list-semirow').addClass('curr');
		$('.ico-list-grid').removeClass('curr');
		var sbHeight = $('.theme-cpcontent').height();
		$('.theme-cpinfo_').css('height',sbHeight+'px');
		$('.theme-cpinfo').css('height',sbHeight+'px');
	});

	$('.list-grid').click(function(e){
		$('.theme-item-content').removeClass('theme-list-content');
		$('.theme-item-content').find('.theme-item').removeClass('con');
		$('.ico-list-semirow').removeClass('curr');
		$('.ico-list-grid').addClass('curr');
		var sbHeight = $('.theme-cpcontent').height();
		$('.theme-cpinfo_').css('height',sbHeight+'px');
		$('.theme-cpinfo').css('height',sbHeight+'px');
	});
	// end toggle item

	//pop up 

	$('#open-popup').click(function(e){
		$('#theme-popup').css('display','block');
		$('#theme-bg').css('display','block');
		$('#theme-bg').animate({'opacity':'0.8'}, 300, 'linear');
		$('#theme-popup').animate({'opacity':'1.00'},300,'linear');
	});
	
	$('#theme-close-popup').click(function(e){
		$('#theme-popup, #theme-bg').animate({'opacity':'0'},300,'linear', function(){$('#theme-popup, #theme-bg').css('display','none');
		});
	});

	$('#theme-bg').click(function(e){
		$('#theme-popup, #theme-bg').animate({'opacity':'0'},300,'linear', function(){$('#theme-popup, #theme-bg').css('display','none');
		});
	});

	$(document).keyup(function(e) {
	    if (e.keyCode == 27) { // Esc
	        $('#theme-popup, #theme-bg').animate({'opacity':'0'},300,'linear', function(){$('#theme-popup, #theme-bg').css('display','none');
			});
	    }
	});

	// end pop up

    // init sidebar height

    var sbHeight = $('.theme-cpcontent').height() - 4;
	$('.theme-cpinfo_').css('height',sbHeight+'px');
	$('.theme-cpinfo').css('height',sbHeight+'px');

	// end init sidebar height

	$('.ico-list-grid').addClass('curr');
}


// show item

function showItem(){
	setTimeout(function () {
		$('.theme-item').animate({
		opacity: 1
		}, {
			duration: 1200,
			queue: false
		});
		$('.theme-item').animate({
			"margin-top": "0px"
		}, {
			duration: 1200,
			specialEasing: {
				"margin-top": "easeOutCirc"
			},
			queue: false
		});
	})
}

// end show item

$(window).on('load',init);


// init fixed components

$(window).scroll(function() {
	var footup = $(document).height() - 1200;
	var toolfix = $(document).height() - 1050;

	if ($(this).scrollTop() > 100){  
		$('.full-container').addClass("theme-cp-fixed");
	}else{
		$('.full-container').removeClass("theme-cp-fixed");
	}

	if ($(this).scrollTop() > footup){  
		$('.full-container').addClass("theme-cp-footermargin");
	}else{
		$('.full-container').removeClass("theme-cp-footermargin");
	}


	if ($(this).scrollTop() > 350 && $(this).scrollTop() < toolfix ){  
		$('.full-container').addClass("theme-cp-fixedtoolbar");
	}else{
		$('.full-container').removeClass("theme-cp-fixedtoolbar");
	}
});

// end init fixed components

// Script Read More
$(document).ready(function() {
     	
	$('.showmore_one').showMore({
		speedDown: 300,
	        speedUp: 300,
	        height: '121px',
	        showText: 'Show more',
	        hideText: 'Show less'
	});
	      
});