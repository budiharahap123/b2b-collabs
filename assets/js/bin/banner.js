if(!window.addEventListener) {
	window.attachEvent('onload', starter);
}else{
	window.addEventListener('load', starter, false);
}
var countslide = 0, countgallery = 0, totalslide = 2, zindx, newZindx, slidebox, slidelist, slideInterval, divText, i ;

function starter() {
	slider('images_container','a', 10000); 
	slider('housetypes','div', 20000);
	//slider('gallery','div', 5000);
}

function slider(id, tag, slideTimer) {
	divText = document.getElementById('textdiv');
	slidebox = document.getElementById(id);
	slidelist = slidebox.getElementsByTagName(tag);
	slide_start(slidelist);
	var slideInterval = setInterval(function() {slide_change(id, tag, 'next')},slideTimer);
}

function slide_start(slidelist) {
	for(var i=0; i<slidelist.length; i++) {
		if(i==0) {slidelist[i].className = 'goshow';}
		slidelist[i].style.zIndex = totalslide-i;
	}
}

function slide_change(id, tag, nextSlide ) {
	slidebox = document.getElementById(id);
	slidelist = slidebox.getElementsByTagName(tag);
	for(i=0; i<slidelist.length; i++) {
		zindx = slidelist[i].style.zIndex;
		if(nextSlide == 'next') {
			newZindx = Number(zindx)+1;	
			if(newZindx > totalslide) { newZindx = (totalslide + 1 -slidelist.length); }
		}else if(nextSlide == 'prev' ){
			newZindx = Number(zindx)-1;
			if(newZindx < (totalslide + 1 -slidelist.length)) { newZindx = totalslide; }
		}
		
		if(newZindx == totalslide) {
			slidelist[i].className = 'goshow';
		}else{
			slidelist[i].className = '';
		}
		
		slidelist[i].style.zIndex = newZindx;
		
	}
}

function sNext() { slide_change('images_container', 'a', 'next');}
function sPrev() { slide_change('images_container', 'a', 'prev');}
function gNext() { slide_change('housetypes', 'div', 'next');}
function gPrev() { slide_change('housetypes', 'div', 'prev');}

// ANCHOR
$('.header a').click(function(){
    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 500);
    return false;
});









