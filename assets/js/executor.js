// ANCHOR SCROLL

$('a').click(function(event){
	var url = $(this) .attr('href');

	//only execute if url contains "#"
	if( (url.search('#')) != -1){


	    $('html, body').animate({
	        scrollTop: $( $.attr(this, 'href') ).offset().top
	    }, 800);

	    event.preventDefault();

	}

});

// PULLDOWN SELECT
$(function(){

		// DROP DOWN SELECTOR
		$.fn.listSelector = function(){

			this.each(function(){

				var obj = $(this);

				$('.option' , obj).on('click' , function(){

					var value = $(this).attr('data-value');
					var text = $(this).text();

					$('.value_set' , obj).val(value);
					$('.cat_display' , obj).html(text);


				});

			});

		};

// HOME NEW LIST

		// PRODUCT GALLERY
		$.fn.prodGallery = function(data){

			this.each(function(){
				var prodinfo = data.productinfo;

				var obj = $(this);
				var change_trigger = $('.trigger' , obj);
				var preloader = '<div class="preloader"></div>';
				var target = $('.imgchange_container' , obj);

				//untuk merubah product description
				function changeProdDesc(e){
					$('.product-info-list' , obj).hide();
					$('.product-info-list.'+e , obj).show();
				}

				//untuk merubah product thumbnail state selected
				function changeThumbState(e){

					change_trigger.removeClass('selected');
					e.addClass('selected');
				}

				//untuk merubah gambar besar
				function changeImg(e){

					target.prepend(preloader);

					$('img' , target).attr('src', e) .load(function() {
						$('.preloader' , target).remove();
					});

				}

				change_trigger.on('click' , function(){
					var this_thumbnail = $(this);
					var target_gambar = this_thumbnail.attr('data-target');

					changeImg(target_gambar);
					changeThumbState(this_thumbnail);

					if(prodinfo == true){
						var prodinfos = this_thumbnail.attr('data-prodinfo');
						changeProdDesc(prodinfos);
					}

				});


			});

		};

});

$(document).ready(function(){
    // initialize selector dropdown untuk searchbar
    $('#cat_selector , #propinsi_selector').listSelector();

});


// JQUERY TAB

$(document).ready(function(){

    $('ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
    })

});


$(document).ready(function(){
	//initiate plugin prodGallery
	$('#prod_gallery_1 , #prod_gallery_2').prodGallery({
		productinfo : true,
	});
});

// TOOL URUTKAN
$(document).ready(function(){
	// initialize selector dropdown untuk searchbar
	$('#tool-urutkan').listSelector();
	$('#tool-kelompok').listSelector();
	$('#tool-sebagai').listSelector();
});


// FAQ
$(document).ready(function(){
    $(".tombola").click(function(){
	  $('body,html').animate({scrollTop: 0}, 800);
      $(".slidingDiv").slideUp("slow");
      $(".umum").slideDown("slow");

    });

    $(".tombolb").click(function(){
	  $('body,html').animate({scrollTop: 0}, 800);
      $(".slidingDiv").slideUp("slow");
      $(".registrasi").slideDown("slow");

    });

    $(".tombolc").click(function(){
	  $('body,html').animate({scrollTop: 0}, 800);
      $(".slidingDiv").slideUp("slow");
      $(".fiturproduk").slideDown("slow");

    });

    $(".tombold").click(function(){
	  $('body,html').animate({scrollTop: 0}, 800);
      $(".slidingDiv").slideUp("slow");
      $(".kategori").slideDown("slow");

    });
});

//Hidden
$('#inner-js, #exec-js, #jquery-js, noscript').remove();


// Lightslider 

$(document).ready(function() {
    $("#lightSlider").lightSlider({
item: 2,
auto: true,
loop:true,
pause:8000,
speed:600,
slideMove: 1,
    }); 
  });

// Lightslider hotlist

$(document).ready(function() {
    $("#lightSlider2").lightSlider({
item: 5,
auto: false,
loop:false,
pause:8000,
speed:600,
slideMove: 1,
    }); 
  });

$(document).ready(function() {
    $("#lightSlider3").lightSlider({
item: 5,
auto: false,
loop:false,
pause:8000,
speed:600,
slideMove: 1,
    }); 
  });
