$(function(){

    // QUICKVIEW
    function showPopUp(){
        $('body').addClass('quickview');
        $(document).keyup(function(e) {
            if (e.keyCode == 27) {
                hidePopUp();
                window.history.back();
            }
        });
    }

    function hidePopUp(){
        $('body').removeClass('quickview');
    //  $(document).unbind("keyup", keyUpFunc);
        $(".overthetop").animate({ scrollTop: 0 }, "fast");
    }

    function showDetailContent(x){
        var target = x;

        showPopUp();

        $('#quickview-box').html(
            '<iframe class="quickview-iframe" src="'+target+'" id="quickview-iframe" scrolling="no"></iframe>'
        );


        $('#quickview-iframe').on('load', function() {
            $('#quickview-iframe').contents().find("head")
              .prepend($('<style type="text/css">.header, .breadcrump, .toolbar, .footer {display:none;} body{margin-top:-137px}</style>'));
            $('#quickview-iframe').contents().find("head")
              .append('<link href="assets/css/quickview.css" media="screen" rel="stylesheet" type="text/css">');
            $('#quickview-iframe').contents().find("#loadJS").append('$(".list-item  a").unbind("click");');
        });

        window.history.pushState("quickview", "page quickview test", target);

    }

    $('.listing-product .list-item  a.link_product').click( function(e){
        e.preventDefault();
        var target = $(this).attr('href');
        showDetailContent(target);

    });

    $('.quickview-nav .close-btn , .overthetop .background').on('click' ,  function(){
        window.history.back();
        hidePopUp();

    });

    if (window.history && window.history.pushState) {
        $(window).on('popstate', function(e) {
            var state = e.originalEvent.state;
            if ( state === null ) {
                    hidePopUp();
            } else {
                    showPopUp();
            }
        });

    }

});

// QUICKVIEW-IFRAME HEIGHT
function iframeloaded() {
    $('#quickview-iframe').load(function() {
        this.style.height = this.contentWindow.document.body.scrollHeight + 'px';
    });
}

// LIST VIEW
$('#list-view ico').click( function() {
    var new_view = $(this).attr('rel');
    var curr_view = $('body').attr('class');
    if(curr_view.search('grid') != -1) { $('body').removeClass('grid'); }
    else if(curr_view.search('semirow') != -1) { $('body').removeClass('semirow'); }
    else if(curr_view.search('row') != -1) { $('body').removeClass('row'); }

    $('body').addClass(new_view);

});
