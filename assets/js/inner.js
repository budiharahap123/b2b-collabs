// STICKY HEADER
$(window).scroll(function() {
    var curr_view = $('body').attr('class');
    var footup = $(document).height() - 1000;
    var toolfix = $(document).height() - 1500;

    if ($(this).scrollTop() > 46){
        $('.totop').addClass("showme");
        $('body').addClass("fixed-margin");
    }else{
        $('.totop').removeClass("showme");
        $('body').removeClass("fixed-margin");
    }
    if ($(this).scrollTop() > footup){
        $('body').addClass("footer-margin");
    }else{
        $('body').removeClass("footer-margin");
    }

    // TOOLBAR
    if(curr_view.search('listing') != -1 && curr_view.search('profile-store') == -1) {
        if ($(this).scrollTop() > 100 && $(this).scrollTop() < toolfix ){
            $('body').addClass("fixed-toolbar");
        }else{
            $('body').removeClass("fixed-toolbar");
        }
        if ($(this).scrollTop() > toolfix ){
            $('body').addClass("footer-toolbar");
        }else{
            $('body').removeClass("footer-toolbar");
        }
    }

    //PROFILE STORE
    if(curr_view.search('profile-store') != -1) {
        if ($(this).scrollTop() > 100){
            $('body').addClass("fixed-profile");
        }else{
            $('body').removeClass("fixed-profile");
        }
        if ($(this).scrollTop() > 270 && $(this).scrollTop() < toolfix ){
            $('body').addClass("fixed-toolbar");
        }else{
            $('body').removeClass("fixed-toolbar");
        }
//        if ($(this).scrollTop() > toolfix ){
//            $('body').addClass("footer-toolbar");
//        }else{
//            $('body').removeClass("footer-toolbar");
//        }
//        if ($(this).scrollTop() > 80 ){
//            $('body').addClass("footer-toolbar");
//        }else{
//            $('body').removeClass("footer-toolbar");
//        }
    }

});


// SLIDE THUMBNAIL DETAIL PAGE
// PRODUCT PAGE GALLERY
$.fn.prodGallerydetail = function(){

    this.each(function(){
        var obj = $(this);
        var thumb_trigger = $('.trigger' , this);
        var nav_trigger = $('.nav' ,this);
        var preloader = $('.preloader' , this);
        var target = $('.imgbox' , this);
        var state = 1;

        function changeImg(i , t){

            target.addClass('loading');

            $('.img' , target).attr('src', i) .load(function() {    target.removeClass('loading');  });

            thumb_trigger.removeClass('on');
            t.addClass('on');
            preloader.addClass('hide');

        }

        thumb_trigger.on('click' , function(){
            var posisi = (($(this).parent('.list').index())+1);
            var url_img = $(this).attr('rel');
            var thumb_target = $(this);
            changeImg(url_img , thumb_target);

            state = posisi;
        });


    });

};

//ADD
//$(document).ready(function() {
//    var max_fields      = 6; //maximum input boxes allowed
//    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
//    var add_button      = $(".add_field_button"); //Add button ID
    
//    var x = 1; //initlal text box count
//    $(add_button).click(function(e){ //on add input button click
//        e.preventDefault();
//        if(x < max_fields){ //max input box allowed
//            x++; //text box increment
//            $(wrapper).prepend('<li><label><div class="imgbox"><img src="assets/images/add-photo.svg"><input type="file"></label><a href="#" class="delete_image_product" >&times;</a></div></li>'); //add input box
//        }
//    });
    
//    $(wrapper).on("click",".delete_image_product", function(e){ //user click on remove text
//        e.preventDefault(); $(this).parent('div').remove(); x--;
//    })
//});
