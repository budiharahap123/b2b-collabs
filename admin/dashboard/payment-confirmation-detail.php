
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("modal.html"); ?>

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Payment Confirmation
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">


          <div class="row">
            <div class="col-md-8">

              <div class="box">

                <table class="table table-striped">
                <tbody>
                <tr>
                  <td>TransactionID</td>
                  <td>123456</td>
                </tr>
                <tr>
                  <td>Username</td>
                  <td>Budiadiliansyah</td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td><a href="">budi.pardomuan@mataharibiz.com</a></td>
                </tr>
                <tr>
                  <td>Nama Perusahaan</td>
                  <td>PT Handal Jaya Berjaya</td>
                </tr>
                <tr>
                  <td>Company Page</td>
                  <td><a href="">http://handaljaya.indonetwork.co.id</a></td>
                </tr>
                <tr>
                  <td>Tipe Layanan</td>
                  <td>Prioritas/Layanan SMS</td>
                </tr>
                <tr>
                  <td>Pembayaran untuk</td>
                  <td>Peningkatan</td>
                </tr>
                <tr>
                  <td>Jumlah Pembayaran</td>
                  <td>Rp 1.700.000</td>
                </tr>
                <tr>
                  <td>Metode Pembayaran</td>
                  <td>Transaksi BCA</td>
                </tr>
                <tr>
                  <td>Status Pembayaran</td>
                  <td>Pending</td>
                </tr>
                <tr>
                  <td> </td>
                  <td><button type="button" class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#myModal">Verifikasi</button> <button type="button" class="btn btn-danger btn-sm" >Tolak</button></td>
                </tr>
              </tbody></table>

              </div>
            
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>



     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
