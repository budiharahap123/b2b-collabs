<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("modal-moderation.html"); ?>

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Edit Produk
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-9">
              <div class="box">
                <div class="product-tab">
                  <div class="tab-pane" id="addproduct">
                  <form method="post" class="form-horizontal">
                    <div class="manage-new-product">
                      <div class="form-group">
                        <div class="col-md-2">Foto produk *</div>
                        <div class="col-md-8">
                            <ul class="foto-produk input_fields_wrap">
                                <?php
                            for ($i=1; $i<=8; $i++) {
                              echo '<li><label><div class="imgbox"><img src="../../assets/data/foto'.$i.'.jpg"><input type="file" value="'.$i.'"></label> <a href="#" class="delete_image_product">×</a></div></li>';
                            }
                            ?>                                               
                              </ul> 
                        </div>
                      </div>
                      <div class="form-group hover-notif-bar">
                        <div class="col-md-2">Nama produk *</div>
                        <div class="col-md-8"><input type="text" class="form-control" value="Lorem Ipsum doloer sit amet"></div>
                        <div class="col-md-12">
                          <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                              <div class="notif-bar">Minimum 20 karakter dan maksimal 155 karakter<br>Nama Produk harus selalu menggunakan huruf kapital. Sedangkan untuk artikel, kata sambung dan komponen lainya harus menggunakan huruf kecil.<br>Contoh : Apple iPhone 4s 32GB - Hitam.</div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-2">Kategori *</div>
                        <div class="col-md-8">
                          <select class="form-control" name="category_product">
                            <option value="">---- Pilih Kategori ----</option>
                            <option value="1|hasChild">Agraris</option>
                            <option value="58|hasChild" selected="">Pakaian &amp; Mode</option>
                            <option value="114|hasChild">Otomotif</option>
                            <option value="188|hasChild">Layanan Bisnis</option>
                            <option value="294|hasChild">Kimia</option>
                            <option value="348|hasChild">Komputer &amp; Software</option>
                            <option value="387|hasChild">Konstruksi &amp; Real Estate</option>
                            <option value="444|hasChild">Elektronik Konsumen</option>
                            <option value="513|hasChild">Elektronik &amp; Elektrik</option>
                            <option value="581|hasChild">Enerji</option>
                            <option value="606|hasChild">Lingkungan</option>
                            <option value="621|hasChild">Persediaan Lebih</option>
                            <option value="637|hasChild">Makanan &amp; Minuman</option>
                            <option value="680|hasChild">Mebel</option>
                            <option value="699|hasChild">Kerajinan &amp; Sovenir</option>
                            <option value="763|hasChild">Kesehatan &amp; Kecantikan</option>
                            <option value="802|hasChild">Kebutuhan Rumah Tangga</option>
                            <option value="853|hasChild">Kebutuhan Industri</option>
                            <option value="922|hasChild">Mineral &amp; Logam</option>
                            <option value="970|hasChild">Kebutuhan Kantor</option>
                            <option value="1022|hasChild">Pembungkus &amp; Kertas</option>
                            <option value="1088|hasChild">Percetakan &amp; Penerbitan</option>
                            <option value="1102|hasChild">Keamanan &amp; Perlindungan</option>
                            <option value="1128|hasChild">Olahraga &amp; Hiburan</option>
                            <option value="1183|hasChild">Tekstil &amp; Kulit</option>
                            <option value="1242|hasChild">Telekomunikasi</option>
                            <option value="1262|hasChild">Mainan</option>
                            <option value="1278|hasChild">Transportasi</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                          <div class="notif-bar">Pilih salah satu kategori dari pilihan di atas. </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                          <select class="form-control" name="category_product">
                            <option value="">---- Pilih Otomotif ----</option>
                            <option value="1|hasChild">Agraris</option>
                            <option value="58|hasChild" selected="">Pakaian &amp; Mode</option>
                            <option value="114|hasChild">Otomotif</option>
                            <option value="188|hasChild">Layanan Bisnis</option>
                            <option value="294|hasChild">Kimia</option>
                            <option value="348|hasChild">Komputer &amp; Software</option>
                            <option value="387|hasChild">Konstruksi &amp; Real Estate</option>
                            <option value="444|hasChild">Elektronik Konsumen</option>
                            <option value="513|hasChild">Elektronik &amp; Elektrik</option>
                            <option value="581|hasChild">Enerji</option>
                            <option value="606|hasChild">Lingkungan</option>
                            <option value="621|hasChild">Persediaan Lebih</option>
                            <option value="637|hasChild">Makanan &amp; Minuman</option>
                            <option value="680|hasChild">Mebel</option>
                            <option value="699|hasChild">Kerajinan &amp; Sovenir</option>
                            <option value="763|hasChild">Kesehatan &amp; Kecantikan</option>
                            <option value="802|hasChild">Kebutuhan Rumah Tangga</option>
                            <option value="853|hasChild">Kebutuhan Industri</option>
                            <option value="922|hasChild">Mineral &amp; Logam</option>
                            <option value="970|hasChild">Kebutuhan Kantor</option>
                            <option value="1022|hasChild">Pembungkus &amp; Kertas</option>
                            <option value="1088|hasChild">Percetakan &amp; Penerbitan</option>
                            <option value="1102|hasChild">Keamanan &amp; Perlindungan</option>
                            <option value="1128|hasChild">Olahraga &amp; Hiburan</option>
                            <option value="1183|hasChild">Tekstil &amp; Kulit</option>
                            <option value="1242|hasChild">Telekomunikasi</option>
                            <option value="1262|hasChild">Mainan</option>
                            <option value="1278|hasChild">Transportasi</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group hover-notif-bar">
                        <div class="col-md-2">Minimum pemesanan *</div>
                        <div class="col-md-4"><input type="text" class="form-control" value="10"></div>
                        <div class="col-md-1">satuan</div>
                        <div class="col-md-3">
                          <select class="form-control" id="unit_id" name="unit_id" tabindex="7">
                                    <option value="">---- Pilih satuan ----</option>
                                    <option value="1">Biji</option>
                                    <option value="2">Buah</option>
                                    <option value="3">Butir</option>
                                    <option value="4" selected="">Ekor</option>
                                    <option value="5">Galon</option>
                                    <option value="6">Gram</option>
                                    <option value="7">Gros</option>
                                    <option value="8">Kantong</option>
                                    <option value="9">Karton</option>
                                    <option value="10">Kilogram (Kg)</option>
                                    <option value="11">Kodi</option>
                                    <option value="12">Kotak</option>
                                    <option value="13">Lembar</option>
                                    <option value="14">Liter</option>
                                    <option value="15">Lusin</option>
                                    <option value="16">Ons</option>
                                    <option value="17">Pak</option>
                                    <option value="18">Pasang</option>
                                    <option value="19">Rantai</option>
                                    <option value="20">Rim</option>
                                    <option value="21">Set</option>
                                    <option value="22">Ton</option>
                                    <option value="23">Unit</option>
                                  </select>
                        </div>
                        <div class="col-md-12">
                          <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-4">
                              <div class="notif-bar">Tulis jumlah dalam angka.</div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group hover-notif-bar">
                        <div class="col-md-2">Harga jual *</div>
                        <div class="col-md-4"><input type="text" class="form-control" value="" placeholder="Rp. 100.000"></div>
                        <div class="col-md-1">Sampai</div>
                        <div class="col-md-3"><input type="text" class="form-control" value="" placeholder="Rp."></div>
                        <div class="col-md-12">
                          <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-4">
                              <div class="notif-bar">Harga terendah.</div>
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-3">
                              <div class="notif-bar">Harga tertinggi.</div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-2">Deskripsi produk *</div>
                        <div class="col-md-8">
                          <script src="../../assets/js/tinymce.min.js"></script>
                                 <script>
                                  tinymce.init({ 
                                    selector:'#txtareatinymce' ,
                                    height: 200,
                                      menubar: false,
                                  });
                                 </script>
                                  <textarea  class="form-control" id="txtareatinymce" tabindex="11" value="Lorem Ipsum doloer sit amet Lorem Ipsum doloer sit amet Lorem Ipsum doloer sit amet Lorem Ipsum doloer sit amet"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                          <div class="notif-bar">
                            - Deskripis produk berisikan mengenai garis besar produk yang akan dijual.<br> - Deskripsi produk terdiri dari minimal 3 - 5 kalimat atau 1 paragraf. <br> - Fitur mengenai produk harus disertakan dengan minimum 3 buah poin utama dari produk tersebut. <br> - Gunakan spasi diantara kita.<br> - Menggunakan kosakata yang jelas. <br> - Tidak boleh ada link tambahan pada deskripsi produk (<i>hyperlink</i>).
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">*wajib diisi</div>
                      </div>
                      <div class="text-center">
                        <input type="submit" class="btn btn-primary" value="Ubah Produk">
                        <a href="acc-manage-listproduk-edit.php" class="btn btn-default">Kembali</a>
                      </div>
                      <br>
                      <br>
                    </div>
                  </form>
                </div>
                </div>
              </div>
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>



     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
