
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Account Management
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
          <div class="col-md-12">
                <div id="exTab2" class="col-md-12"> 
                  <ul class="nav nav-tabs tabs-edited">
                    <li class="active">
                      <a  href="#1" data-toggle="tab">Product</a>
                    </li>
                    <li><a href="#2" data-toggle="tab">Themes</a></li>
                    <li><a href="#3" data-toggle="tab">Running Text</a></li>
                    <li><a href="#4" data-toggle="tab">Upgrade</a></li>
                  </ul>
                    <div class="box">
                      <div class="tab-content">
                        <div class="tab-pane active" id="1">
                          <div class="box-header with-border">
                            <h3 class="box-title">Total Poin Anda : <span class="orange">500 poin</span></h3>
                          </div>
                          <div class="box-body no-padding">
                            <div id="exTab2" class="product-tab">
                              <ul class="nav nav-tabs">
                                <li class="active"><a href="#listproduct" data-toggle="tab">List Produk</a></li>
                                <li><a href="#addproduct" data-toggle="tab">Tambah Produk Baru</a></li>
                                <li ><a  href="#sundul" data-toggle="tab">Sundul</a></li>
                                <li><a href="#hotlist" data-toggle="tab">Hot List</a></li>
                              </ul>
                              <div class="product-tab-content">
                                <div class="tab-content">
                                  <div class="tab-pane active" id="listproduct">
                                    <div class="box-body">
                                      <div class="row">
                                        <div class="col-md-8">
                                          <div class="input-group">
                                            <input type="text" class="form-control">
                                              <div class="input-group-btn">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Title Produk
                                                  &nbsp;&nbsp;<span class="fa fa-caret-down"></span></button>
                                                <ul class="dropdown-menu">
                                                  <li><a href="#">Title Produk</a></li>
                                                  <li><a href="#">Product Desc (id)</a></li>
                                                  <li><a href="#">Product Desc (eng)</a></li>
                                                  <li><a href="#">Tanggal dibuat</a></li>
                                                </ul>
                                                <button type="button" class="btn btn-primary">Cari</button>
                                              </div>
                                              <!-- /btn-group -->
                                          </div>
                                        </div><!-- /.col -->
                                      </div><!-- /.row -->
                                      <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                        <div class="row">
                                          <div class="col-sm-12">
                                            <div class="dataTables_length" id="example1_length">
                                              <label>Tampilkan&nbsp;&nbsp;<select name="example1_length" aria-controls="example1" class="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select>&nbsp;&nbsp;data</label>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-12">
                                          <table id="example1" class="table table-bordered table-hover">
                                            <tr>
                                              <th>ID</th>
                                              <th class="col-xs-1">Tanggal dibuat</th>
                                              <th class="col-xs-2">Judul Produk</th>
                                              <th class="col-xs-5">Deskripsi Produk</th>
                                              <th class="col-xs-1">Kategori</th>
                                              <th class="col-xs-1">Status</th>
                                              <th class="col-xs-1" colspan="2">Action</th>
                                            </tr>
                                            <tr>
                                              <td>1234</td>
                                              <td>10/01/2016</td>
                                              <td><a href="">Baju Branding</a></td>
                                              <td><small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small></td>
                                              <td>Komputer</td>
                                              <td>Aktif</td>
                                              <td><div class="btn-group"><a href="acc-manage-listproduk-editing.php" class="btn btn-flat btn-xs btn-primary">Edit</a></div></td>
                                               <td><div class="btn-group"><a href="acc-manage-listproduk-edit.php" class="btn btn-flat btn-xs btn-danger">Detail</a></div></td>
                                            </tr>
                                            <tr>
                                              <td>1234</td>
                                              <td>10/01/2016</td>
                                              <td><a href="">Baju Branding</a></td>
                                              <td><small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small></td>
                                              <td>Komputer</td>
                                              <td>Aktif</td>
                                              <td><div class="btn-group"><a href="acc-manage-listproduk-editing.php" class="btn btn-flat btn-xs btn-primary">Edit</a></div></td>
                                               <td><div class="btn-group"><a href="acc-manage-listproduk-edit.php" class="btn btn-flat btn-xs btn-danger">Detail</a></div></td>
                                            </tr><tr>
                                              <td>1234</td>
                                              <td>10/01/2016</td>
                                              <td><a href="">Baju Branding</a></td>
                                              <td><small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small></td>
                                              <td>Komputer</td>
                                              <td>Aktif</td>
                                              <td><div class="btn-group"><a href="acc-manage-listproduk-editing.php" class="btn btn-flat btn-xs btn-primary">Edit</a></div></td>
                                               <td><div class="btn-group"><a href="acc-manage-listproduk-edit.php" class="btn btn-flat btn-xs btn-danger">Detail</a></div></td>
                                            </tr><tr>
                                              <td>1234</td>
                                              <td>10/01/2016</td>
                                              <td><a href="">Baju Branding</a></td>
                                              <td><small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small></td>
                                              <td>Komputer</td>
                                              <td>Aktif</td>
                                              <td><div class="btn-group"><a href="acc-manage-listproduk-editing.php" class="btn btn-flat btn-xs btn-primary">Edit</a></div></td>
                                               <td><div class="btn-group"><a href="acc-manage-listproduk-edit.php" class="btn btn-flat btn-xs btn-danger">Detail</a></div></td>
                                            </tr><tr>
                                              <td>1234</td>
                                              <td>10/01/2016</td>
                                              <td><a href="">Baju Branding</a></td>
                                              <td><small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small></td>
                                              <td>Komputer</td>
                                              <td>Aktif</td>
                                              <td><div class="btn-group"><a href="acc-manage-listproduk-editing.php" class="btn btn-flat btn-xs btn-primary">Edit</a></div></td>
                                               <td><div class="btn-group"><a href="acc-manage-listproduk-edit.php" class="btn btn-flat btn-xs btn-danger">Detail</a></div></td>
                                            </tr><tr>
                                              <td>1234</td>
                                              <td>10/01/2016</td>
                                              <td><a href="">Baju Branding</a></td>
                                              <td><small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small></td>
                                              <td>Komputer</td>
                                              <td>Aktif</td>
                                              <td><div class="btn-group"><a href="acc-manage-listproduk-editing.php" class="btn btn-flat btn-xs btn-primary">Edit</a></div></td>
                                               <td><div class="btn-group"><a href="acc-manage-listproduk-edit.php" class="btn btn-flat btn-xs btn-danger">Detail</a></div></td>
                                            </tr><tr>
                                              <td>1234</td>
                                              <td>10/01/2016</td>
                                              <td><a href="">Baju Branding</a></td>
                                              <td><small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small></td>
                                              <td>Komputer</td>
                                              <td>Aktif</td>
                                              <td><div class="btn-group"><a href="acc-manage-listproduk-editing.php" class="btn btn-flat btn-xs btn-primary">Edit</a></div></td>
                                               <td><div class="btn-group"><a href="acc-manage-listproduk-edit.php" class="btn btn-flat btn-xs btn-danger">Detail</a></div></td>
                                            </tr><tr>
                                              <td>1234</td>
                                              <td>10/01/2016</td>
                                              <td><a href="">Baju Branding</a></td>
                                              <td><small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small></td>
                                              <td>Komputer</td>
                                              <td>Aktif</td>
                                              <td><div class="btn-group"><a href="acc-manage-listproduk-editing.php" class="btn btn-flat btn-xs btn-primary">Edit</a></div></td>
                                               <td><div class="btn-group"><a href="acc-manage-listproduk-edit.php" class="btn btn-flat btn-xs btn-danger">Detail</a></div></td>
                                            </tr><tr>
                                              <td>1234</td>
                                              <td>10/01/2016</td>
                                              <td><a href="">Baju Branding</a></td>
                                              <td><small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small></td>
                                              <td>Komputer</td>
                                              <td>Aktif</td>
                                              <td><div class="btn-group"><a href="acc-manage-listproduk-editing.php" class="btn btn-flat btn-xs btn-primary">Edit</a></div></td>
                                               <td><div class="btn-group"><a href="acc-manage-listproduk-edit.php" class="btn btn-flat btn-xs btn-danger">Detail</a></div></td>
                                            </tr><tr>
                                              <td>1234</td>
                                              <td>10/01/2016</td>
                                              <td><a href="">Baju Branding</a></td>
                                              <td><small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small></td>
                                              <td>Komputer</td>
                                              <td>Aktif</td>
                                              <td><div class="btn-group"><a href="acc-manage-listproduk-editing.php" class="btn btn-flat btn-xs btn-primary">Edit</a></div></td>
                                               <td><div class="btn-group"><a href="acc-manage-listproduk-edit.php" class="btn btn-flat btn-xs btn-danger">Detail</a></div></td>
                                            </tr>
                                            
                                          </table>
                                        </div>
                                        <div class="col-md-12">
                                          <div class="col-sm-5">
                                            <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
                                          </div>
                                          <div class="col-sm-7">
                                            <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                                              <ul class="pagination">
                                                <li class="paginate_button previous disabled" id="example1_previous"><a href="#" aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a></li>
                                                <li class="paginate_button active"><a href="#" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a></li>
                                                <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a></li>
                                                <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" tabindex="0">3</a></li>
                                                <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="4" tabindex="0">4</a></li>
                                                <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="5" tabindex="0">5</a></li>
                                                <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="6" tabindex="0">6</a></li>
                                                <li class="paginate_button next" id="example1_next"><a href="#" aria-controls="example1" data-dt-idx="7" tabindex="0">Next</a></li>
                                              </ul>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="tab-pane" id="addproduct">
                                    <form method="post" class="form-horizontal">
                                      <div class="manage-new-product">
                                        <div class="form-group">
                                          <div class="col-md-2">Foto produk *</div>
                                          <div class="col-md-8">
                                            <ul class="foto-produk input_fields_wrap">
                                              <li>
                                                <label>
                                                  <div class="imgbox"><img src="../../assets/images/add-photo.svg">
                                                    <input type="file" value="1">
                                                    <a href="#" class="delete_image_product">×</a>
                                                  </div>
                                                </label>
                                              </li>
                                              <li>
                                                <label>
                                                  <div class="imgbox"><img src="../../assets/images/add-photo.svg">
                                                    <input type="file" value="2">
                                                    <a href="#" class="delete_image_product">×</a>
                                                  </div>
                                                </label>
                                              </li>
                                              <li>
                                                <label>
                                                  <div class="imgbox"><img src="../../assets/images/add-photo.svg">
                                                    <input type="file" value="3">
                                                    <a href="#" class="delete_image_product">×</a>
                                                  </div>
                                                </label>
                                              </li>
                                              <li>
                                                <label>
                                                  <div class="imgbox"><img src="../../assets/images/add-photo.svg">
                                                    <input type="file" value="4">
                                                    <a href="#" class="delete_image_product">×</a>
                                                  </div>
                                                </label>
                                              </li>
                                              <li>
                                                <label>
                                                  <div class="imgbox"><img src="../../assets/images/add-photo.svg">
                                                    <input type="file" value="5">
                                                    <a href="#" class="delete_image_product">×</a>
                                                  </div>
                                                </label>
                                              </li>
                                              <li>
                                                <label>
                                                  <div class="imgbox"><img src="../../assets/images/add-photo.svg">
                                                    <input type="file" value="6">
                                                    <a href="#" class="delete_image_product">×</a>
                                                  </div>
                                                </label>
                                              </li>
                                              <li>
                                                <label>
                                                  <div class="imgbox"><img src="../../assets/images/add-photo.svg">
                                                    <input type="file" value="7">
                                                    <a href="#" class="delete_image_product">×</a>
                                                  </div>
                                                </label>
                                              </li>
                                              <li>
                                                <label>
                                                  <div class="imgbox"><img src="../../assets/images/add-photo.svg">
                                                    <input type="file" value="8">
                                                    <a href="#" class="delete_image_product">×</a>
                                                  </div>
                                                </label>
                                              </li>                               
                                            </ul>
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <div class="col-md-2">Tipe *</div>
                                          <div class="col-md-8">
                                            <select class="form-control" name="category_product">
                                              <option value="">---- Pilih ----</option>
                                              <option value="">Katalog Produk</option>
                                              <option value="">Penawaran Jual</option>
                                              <option value="">Penawaran Beli</option>
                                              <option value="">Penawaran Kerjasama</option>
                                                  </select>
                                          </div>
                                        </div>
                                        <div class="form-group hover-notif-bar">
                                          <div class="col-md-2">Nama produk *</div>
                                          <div class="col-md-8"><input type="text" class="form-control" value=""></div>
                                          <div class="col-md-12">
                                            <div class="row">
                                              <div class="col-md-2"></div>
                                              <div class="col-md-8">
                                                <div class="notif-bar">Minimum 20 karakter dan maksimal 155 karakter<br>Nama Produk harus selalu menggunakan huruf kapital. Sedangkan untuk artikel, kata sambung dan komponen lainya harus menggunakan huruf kecil.<br>Contoh : Apple iPhone 4s 32GB - Hitam.</div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <div class="col-md-2">Kategori *</div>
                                          <div class="col-md-8">
                                            <select class="form-control" name="category_product">
                                              <option value="">---- Pilih Kategori ----</option>
                                              <option value="1|hasChild">Agraris</option>
                                              <option value="58|hasChild">Pakaian &amp; Mode</option>
                                              <option value="114|hasChild">Otomotif</option>
                                              <option value="188|hasChild">Layanan Bisnis</option>
                                              <option value="294|hasChild">Kimia</option>
                                              <option value="348|hasChild">Komputer &amp; Software</option>
                                              <option value="387|hasChild">Konstruksi &amp; Real Estate</option>
                                              <option value="444|hasChild">Elektronik Konsumen</option>
                                              <option value="513|hasChild">Elektronik &amp; Elektrik</option>
                                              <option value="581|hasChild">Enerji</option>
                                              <option value="606|hasChild">Lingkungan</option>
                                              <option value="621|hasChild">Persediaan Lebih</option>
                                              <option value="637|hasChild">Makanan &amp; Minuman</option>
                                              <option value="680|hasChild">Mebel</option>
                                              <option value="699|hasChild">Kerajinan &amp; Sovenir</option>
                                              <option value="763|hasChild">Kesehatan &amp; Kecantikan</option>
                                              <option value="802|hasChild">Kebutuhan Rumah Tangga</option>
                                              <option value="853|hasChild">Kebutuhan Industri</option>
                                              <option value="922|hasChild">Mineral &amp; Logam</option>
                                              <option value="970|hasChild">Kebutuhan Kantor</option>
                                              <option value="1022|hasChild">Pembungkus &amp; Kertas</option>
                                              <option value="1088|hasChild">Percetakan &amp; Penerbitan</option>
                                              <option value="1102|hasChild">Keamanan &amp; Perlindungan</option>
                                              <option value="1128|hasChild">Olahraga &amp; Hiburan</option>
                                              <option value="1183|hasChild">Tekstil &amp; Kulit</option>
                                              <option value="1242|hasChild">Telekomunikasi</option>
                                              <option value="1262|hasChild">Mainan</option>
                                              <option value="1278|hasChild">Transportasi</option>
                                            </select>
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <div class="col-md-2"></div>
                                          <div class="col-md-8">
                                            <div class="notif-bar">Pilih salah satu kategori dari pilihan di atas. </div>
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <div class="col-md-2"></div>
                                          <div class="col-md-8">
                                            <select class="form-control" name="category_product">
                                              <option value="">---- Pilih Otomotif ----</option>
                                              <option value="1|hasChild">Agraris</option>
                                              <option value="58|hasChild">Pakaian &amp; Mode</option>
                                              <option value="114|hasChild">Otomotif</option>
                                              <option value="188|hasChild">Layanan Bisnis</option>
                                              <option value="294|hasChild">Kimia</option>
                                              <option value="348|hasChild">Komputer &amp; Software</option>
                                              <option value="387|hasChild">Konstruksi &amp; Real Estate</option>
                                              <option value="444|hasChild">Elektronik Konsumen</option>
                                              <option value="513|hasChild">Elektronik &amp; Elektrik</option>
                                              <option value="581|hasChild">Enerji</option>
                                              <option value="606|hasChild">Lingkungan</option>
                                              <option value="621|hasChild">Persediaan Lebih</option>
                                              <option value="637|hasChild">Makanan &amp; Minuman</option>
                                              <option value="680|hasChild">Mebel</option>
                                              <option value="699|hasChild">Kerajinan &amp; Sovenir</option>
                                              <option value="763|hasChild">Kesehatan &amp; Kecantikan</option>
                                              <option value="802|hasChild">Kebutuhan Rumah Tangga</option>
                                              <option value="853|hasChild">Kebutuhan Industri</option>
                                              <option value="922|hasChild">Mineral &amp; Logam</option>
                                              <option value="970|hasChild">Kebutuhan Kantor</option>
                                              <option value="1022|hasChild">Pembungkus &amp; Kertas</option>
                                              <option value="1088|hasChild">Percetakan &amp; Penerbitan</option>
                                              <option value="1102|hasChild">Keamanan &amp; Perlindungan</option>
                                              <option value="1128|hasChild">Olahraga &amp; Hiburan</option>
                                              <option value="1183|hasChild">Tekstil &amp; Kulit</option>
                                              <option value="1242|hasChild">Telekomunikasi</option>
                                              <option value="1262|hasChild">Mainan</option>
                                              <option value="1278|hasChild">Transportasi</option>
                                            </select>
                                          </div>
                                        </div>
                                        <div class="form-group hover-notif-bar">
                                          <div class="col-md-2">Minimum pemesanan *</div>
                                          <div class="col-md-4"><input type="text" class="form-control" value=""></div>
                                          <div class="col-md-1">satuan</div>
                                          <div class="col-md-3">
                                            <select class="form-control" id="unit_id" name="unit_id" tabindex="7">
                                                      <option value="">---- Pilih satuan ----</option>
                                                      <option value="1">Biji</option>
                                                      <option value="2">Buah</option>
                                                      <option value="3">Butir</option>
                                                      <option value="4">Ekor</option>
                                                      <option value="5">Galon</option>
                                                      <option value="6">Gram</option>
                                                      <option value="7">Gros</option>
                                                      <option value="8">Kantong</option>
                                                      <option value="9">Karton</option>
                                                      <option value="10">Kilogram (Kg)</option>
                                                      <option value="11">Kodi</option>
                                                      <option value="12">Kotak</option>
                                                      <option value="13">Lembar</option>
                                                      <option value="14">Liter</option>
                                                      <option value="15">Lusin</option>
                                                      <option value="16">Ons</option>
                                                      <option value="17">Pak</option>
                                                      <option value="18">Pasang</option>
                                                      <option value="19">Rantai</option>
                                                      <option value="20">Rim</option>
                                                      <option value="21">Set</option>
                                                      <option value="22">Ton</option>
                                                      <option value="23">Unit</option>
                                                    </select>
                                          </div>
                                          <div class="col-md-12">
                                            <div class="row">
                                              <div class="col-md-2"></div>
                                              <div class="col-md-4">
                                                <div class="notif-bar">Tulis jumlah dalam angka.</div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="form-group hover-notif-bar">
                                          <div class="col-md-2">Harga jual *</div>
                                          <div class="col-md-4"><input type="text" class="form-control" value="" placeholder="Rp."></div>
                                          <div class="col-md-1">Sampai</div>
                                          <div class="col-md-3"><input type="text" class="form-control" value="" placeholder="Rp."></div>
                                          <div class="col-md-12">
                                            <div class="row">
                                              <div class="col-md-2"></div>
                                              <div class="col-md-4">
                                                <div class="notif-bar">Harga terendah.</div>
                                              </div>
                                              <div class="col-md-1"></div>
                                              <div class="col-md-3">
                                                <div class="notif-bar">Harga tertinggi.</div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <div class="col-md-2">Deskripsi produk *</div>
                                          <div class="col-md-8">
                                            <script src="../../assets/js/tinymce.min.js"></script>
                                                   <script>
                                                    tinymce.init({ 
                                                      selector:'#txtareatinymce' ,
                                                      height: 200,
                                                        menubar: false,
                                                    });
                                                   </script>
                                                    <textarea  class="form-control" id="txtareatinymce" tabindex="11"></textarea>
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <div class="col-md-2"></div>
                                          <div class="col-md-8">
                                            <div class="notif-bar">
                                              - Deskripis produk berisikan mengenai garis besar produk yang akan dijual.<br> - Deskripsi produk terdiri dari minimal 3 - 5 kalimat atau 1 paragraf. <br> - Fitur mengenai produk harus disertakan dengan minimum 3 buah poin utama dari produk tersebut. <br> - Gunakan spasi diantara kita.<br> - Menggunakan kosakata yang jelas. <br> - Tidak boleh ada link tambahan pada deskripsi produk (<i>hyperlink</i>).
                                            </div>
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <div class="col-md-2"></div>
                                          <div class="col-md-8">*wajib diisi</div>
                                        </div>
                                        <div class="text-center">
                                          <input type="submit" class="btn btn-primary" value="Tambah Produk">
                                        </div>
                                      </div>
                                    </form>
                                  </div>
                                  <div class="tab-pane" id="sundul">
                                    <form class="form-horizontal">
                                      <div class="row">
                                        <div class="col-md-5">
                                          <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Cari Produk">
                                            <span class="input-group-btn">
                                              <button class="btn btn-primary btn-flat" type="button">Search</button>
                                            </span>
                                          </div>
                                        </div>
                                        <div class="col-sm-12">
                                          <table id="example2" class="table table-bordered table-hover" >
                                            <tr>
                                              <th class="col-md-1"></th>
                                              <th class="col-md-3">Photo</th>
                                              <th class="col-md-5">Judul Produk</th>
                                              <th class="col-md-3">Poin</th>
                                            </tr>
                                            <tr>
                                              <td>
                                                <label title=" Centang jika ingin merubah harga"><input type="checkbox" class="edtprc" name="sundul produk" value="2" onclick="this.form.total.value=checkChoice(this);"></label>
                                              </td>
                                              <td><a href=""><img src="../dist/img/photo1.png" width="100"></a></td>
                                              <td>Nike Lunar Glide 7</td>
                                              <td>2</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <label title=" Centang jika ingin merubah harga"><input type="checkbox" class="edtprc" name="sundul produk" value="2" onclick="this.form.total.value=checkChoice(this);"></label>
                                              </td>
                                              <td><a href=""><img src="../dist/img/photo2.png" width="100"></a></td>
                                              <td>Nike Lunar Glide 7</td>
                                              <td>2</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <label title=" Centang jika ingin merubah harga"><input type="checkbox" class="edtprc" name="sundul produk" value="2" onclick="this.form.total.value=checkChoice(this);"></label>
                                              </td>
                                              <td><a href=""><img src="../dist/img/photo3.jpg" width="100"></a></td>
                                              <td>Nike Lunar Glide 7</td>
                                              <td>2</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <label title=" Centang jika ingin merubah harga"><input type="checkbox" class="edtprc" name="sundul produk" value="2" onclick="this.form.total.value=checkChoice(this);"></label>
                                              </td>
                                              <td><a href=""><img src="../dist/img/photo4.jpg" width="100"></a></td>
                                              <td>Nike Lunar Glide 7</td>
                                              <td>2</td>
                                            </tr>
                                          </table>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-5">
                                          <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
                                        </div>
                                        <div class="col-sm-7">
                                          <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                                            <ul class="pagination">
                                              <li class="paginate_button previous disabled" id="example1_previous"><a href="#" aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a></li>
                                              <li class="paginate_button active"><a href="#" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a></li>
                                              <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a></li>
                                              <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" tabindex="0">3</a></li>
                                              <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="4" tabindex="0">4</a></li>
                                              <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="5" tabindex="0">5</a></li>
                                              <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="6" tabindex="0">6</a></li>
                                              <li class="paginate_button next" id="example1_next"><a href="#" aria-controls="example1" data-dt-idx="7" tabindex="0">Next</a></li>
                                            </ul>
                                          </div>
                                        </div>
                                      </div>
                                      <div>
                                        <div class="form-group">
                                          <div class="col-md-12">Poin Yang di Butuhkan</div>
                                          <div class="col-md-12"><input type="text" class="form-control" value="0"></div>
                                        </div>
                                        <div class="form-group">
                                          <div class="col-md-12">Sisa Poin Anda</div>
                                          <div class="col-md-12"><input type="text" class="form-control" value="200"></div>
                                        </div>
                                      </div>
                                      <div class="text-center">
                                        <input type="submit" value="Sundul" class="btn btn-primary">
                                      </div>
                                    </form>
                                  </div>
                                  <div class="tab-pane" id="hotlist">
                                    <form class="form-horizontal">
                                      <div class="row">
                                        <div class="col-md-5">
                                          <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Cari Produk">
                                            <span class="input-group-btn">
                                              <button class="btn btn-primary btn-flat" type="button">Search</button>
                                            </span>
                                          </div>
                                        </div>
                                        <div class="col-sm-12">
                                          <table id="example2" class="table table-bordered table-hover" >
                                            <tr>
                                              <th class="col-md-1"></th>
                                              <th class="col-md-3">Photo</th>
                                              <th class="col-md-5">Judul Produk</th>
                                              <th class="col-md-3">Poin</th>
                                            </tr>
                                            <tr>
                                              <td>
                                                <label title=" Centang jika ingin merubah harga"><input type="checkbox" class="edtprc" name="sundul produk" value="2" onclick="this.form.total.value=checkChoice(this);"></label>
                                              </td>
                                              <td><a href=""><img src="../dist/img/photo1.png" width="100"></a></td>
                                              <td>Nike Lunar Glide 7</td>
                                              <td>2</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <label title=" Centang jika ingin merubah harga"><input type="checkbox" class="edtprc" name="sundul produk" value="2" onclick="this.form.total.value=checkChoice(this);"></label>
                                              </td>
                                              <td><a href=""><img src="../dist/img/photo2.png" width="100"></a></td>
                                              <td>Nike Lunar Glide 7</td>
                                              <td>2</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <label title=" Centang jika ingin merubah harga"><input type="checkbox" class="edtprc" name="sundul produk" value="2" onclick="this.form.total.value=checkChoice(this);"></label>
                                              </td>
                                              <td><a href=""><img src="../dist/img/photo3.jpg" width="100"></a></td>
                                              <td>Nike Lunar Glide 7</td>
                                              <td>2</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <label title=" Centang jika ingin merubah harga"><input type="checkbox" class="edtprc" name="sundul produk" value="2" onclick="this.form.total.value=checkChoice(this);"></label>
                                              </td>
                                              <td><a href=""><img src="../dist/img/photo4.jpg" width="100"></a></td>
                                              <td>Nike Lunar Glide 7</td>
                                              <td>2</td>
                                            </tr>
                                          </table>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-5">
                                          <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
                                        </div>
                                        <div class="col-sm-7">
                                          <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                                            <ul class="pagination">
                                              <li class="paginate_button previous disabled" id="example1_previous"><a href="#" aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a></li>
                                              <li class="paginate_button active"><a href="#" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a></li>
                                              <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a></li>
                                              <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" tabindex="0">3</a></li>
                                              <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="4" tabindex="0">4</a></li>
                                              <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="5" tabindex="0">5</a></li>
                                              <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="6" tabindex="0">6</a></li>
                                              <li class="paginate_button next" id="example1_next"><a href="#" aria-controls="example1" data-dt-idx="7" tabindex="0">Next</a></li>
                                            </ul>
                                          </div>
                                        </div>
                                      </div>
                                      <div>
                                        <div class="form-group">
                                          <div class="col-md-12">Poin Yang di Butuhkan</div>
                                          <div class="col-md-12"><input type="text" class="form-control" value="0"></div>
                                        </div>
                                        <div class="form-group">
                                          <div class="col-md-12">Sisa Poin Anda</div>
                                          <div class="col-md-12"><input type="text" class="form-control" value="200"></div>
                                        </div>
                                      </div>
                                      <div class="text-center">
                                        <input type="submit" value="Hot List" class="btn btn-primary">
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane" id="2">
                          <div class="dashboard-display col-md-12">
                            <div>
                              <div class="dashboard-detail">
                                <br>
                                <div class="row">
                                  <ul class="list switchtemplate col-md-12">
                                              <li class="container col-md-12">
                                                <div>
                                                <div class="col-md-6 img-template-now"><img src="../../assets/images/template.jpg" width="100%"></div>
                                                <div class="col-md-6 template-now">Template yang sekarang digunakan:<h3>Default Company Profile</h3><br /><a href="./?page=profile-store" class="btn btn-block btn-default">Lihat Halaman Saya</a></div></div>
                                              </li>
                                              <br>
                                                  <li class="col-md-3"><div class="col-md-12"><div class="bgtemp"><img src="../../assets/images/tema01/template.jpg" width="100%"></div><div><a href="./assets/images/tema01/index.html" class="switch btn">Gunakan Template</a></div><h3>01. Nama Template</h3></div></li>
                                                  <li class="col-md-3"><div class="col-md-12"><div class="bgtemp"><img src="../../assets/images/tema02/template.jpg" width="100%"></div><div><a href="./assets/images/tema02/index.html" class="switch btn">Gunakan Template</a></div><h3>01. Nama Template</h3></div></li>
                                                  <li class="col-md-3"><div class="col-md-12"><div class="bgtemp"><img src="../../assets/images/tema03/template.jpg" width="100%"></div><div><a href="./assets/images/tema03/index.html" class="switch btn">Gunakan Template</a></div><h3>02. Nama Template</h3></div></li>
                                                  <li class="col-md-3"><div class="col-md-12"><div class="bgtemp"><img src="../../assets/images/tema04/template.jpg" width="100%"></div><div><a href="./assets/images/tema04/index.html" class="switch btn">Gunakan Template</a></div><h3>03. Nama Template</h3></div></li>
                                              </li>
                                          </ul>
                                        </div>
                                
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane" id="3">
                          <form class="form-horizontal">
                            <div class="box-body">
                              <div class="form-group">
                                <label class="col-sm-2">Status Running Text</label>
                                <div class="col-sm-10">Aktif</div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-2">Pilihan Running Text</label>
                                <div class="col-sm-10">
                                  <select class="form-control">
                                    <option>-</option>
                                    <option selected>Running text unik</option>
                                    <option>Nama perusahaan</option>
                                    <option>Judul produk terakhir</option>
                                  </select>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-2">Running Text Unik</label>
                                <div class="col-sm-10"><textarea class="form-control" placeholder="Tulis Running Text Unik Disini"></textarea></div>
                              </div>
                              <div class="text-center">
                                <input type="submit" value="Aktifkan / Nonaktifkan" class="btn btn-primary">
                              </div>
                            </div>
                          </form>
                        </div>
                        <div class="tab-pane" id="4">
                          <div id="exTab2" class="product-tab">
                            <ul class="nav nav-tabs">
                              <li class="active"><a href="#perpanjang-keanggotaan" data-toggle="tab">Perpanjang Keanggotaan</a></li>
                              <li><a href="#list-upgrade" data-toggle="tab">List Permintaan Upgrade</a></li>
                            </ul>
                            <div class="tab-content">
                              <div class="tab-pane active" id="perpanjang-keanggotaan">
                                <div class="box-body">
                                 <h3 class="box-title">Hi, Akun akun</h3>
                                 <p>Saat ini status keanggotaan anda adalah:   <b>Anggota Prioritas</b></p>
                                 <div class="col-md-12">
                                    <table id="example1" class="table table-bordered table-striped">
                                      <tr>
                                        <th class="col-xs-12" colspan="3">Info Status</th>
                                      </tr>
                                      <tr>
                                        <td class="col-xs-8">Tanggal Berakhir Keanggotaan </td>
                                        <td class="col-xs-2">10/01/2016</td>
                                        <td class="col-xs-3"><div class="btn-group"><a href="upgrade-step1.php" class="btn btn-block btn-warning">Perpanjang Sekarang</a></div></td>
                                      </tr>
                                    </table>
                                  </div>
                                </div>
                              </div>
                              <div class="tab-pane" id="list-upgrade">
                                <div class="box-body">
                                  <div class="row">
                                    <div class="col-md-12">
                                      <table id="example1" class="table table-bordered table-hover">
                                        <tr>
                                          <th class="col-xs-1">ID</th>
                                          <th class="col-xs-1">Tanggal</th>
                                          <th class="col-xs-1">Keanggotaan</th>
                                          <th class="col-xs-2">Status</th>
                                          <th class="col-xs-2">Jumlah Pembayaran</th>
                                          <th class="col-xs-2">Jumlah Dibayar</th>
                                          <th class="col-xs-1">Tipe Bayar</th>
                                          <th class="col-xs-1"></th>
                                        </tr>
                                        <tr>
                                          <td>48</td>
                                          <td>25-01-2018</td>
                                          <td>Prioritas</td>
                                          <td>Menunggu Pembayaran</td>
                                          <td>IDR 1.700.000</td>
                                          <td>IDR 0</td>
                                          <td>BCA</td>
                                          <td><a href="upgrade-step3.php" class="btn btn-block btn-warning">Konfirmasi</a></td>
                                        </tr>
                                        <tr>
                                          <td>100</td>
                                          <td>25-01-2018</td>
                                          <td>Prioritas</td>
                                          <td>Menunggu Pembayaran</td>
                                          <td>IDR 1.700.000</td>
                                          <td>IDR 1.000.000</td>
                                          <td>BRI</td>
                                          <td><a href="upgrade-step3.php" class="btn btn-block btn-warning">Konfirmasi</a></td>
                                        </tr>
                                      </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          
                        </div>
                      </div>
                    </div>
                </div>
          </div>
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>



     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
