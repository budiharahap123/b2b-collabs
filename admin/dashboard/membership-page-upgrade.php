
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("modal.html"); ?>

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Upgrade Membership
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">


          <div class="row">
            <div class="col-md-8">

              <div class="box">

                <form>

                <table class="table table-striped">
                <tbody>
                <tr>
                  <td>UserID</td>
                  <td>123456</td>
                </tr>
                <tr>
                  <td>Username</td>
                  <td>Budiadiliansyah</td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td><a href="">budi.pardomuan@mataharibiz.com</a></td>
                </tr>
                <tr>
                  <td>Nama Perusahaan</td>
                  <td>PT Handal Jaya Berjaya</td>
                </tr>
                <tr>
                  <td>Company Page</td>
                  <td><a href="">http://handaljaya.indonetwork.co.id</a></td>
                </tr>
                <tr>
                  <td>Membership Type</td>
                  <td>Prioritas</td>
                </tr>
                <tr>
                  <td>Membership Expiring</td>
                  <td>
                    <div><label for="inputEmail3" class="control-label">10 Agustus 2016&nbsp;&nbsp;&nbsp;</label></div>
                    <div class="pull-left">
                      <label for="inputEmail3" class="control-label">Tambahkan</label>&nbsp;&nbsp;&nbsp;
                    </div>
                    <div class="pull-left">
                        <select class="form-control">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                    </select>
                  </div>
                    <div class="pull-left">&nbsp;&nbsp;&nbsp;
                      <label for="inputEmail3" class="control-label">Tahun</label>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Unggah File</td>
                  <td>
                    <label for="exampleInputFile">File input</label>
                    <input type="file" id="exampleInputFile">
                    <p class="help-block">Example block-level help text here.</p>
                  </td>
                </tr>
                <tr>
                  <td> </td>
                  <td><button type="button" class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#myModal">Proses</button> <button type="button" class="btn btn-danger btn-sm" >Batal</button></td>
                </tr>
              </tbody></table>

              </form>



              </div>
            
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>


<script>
$('#myModal').on('shown.bs.modal', function () {
  $('#myInput').focus()
})
</script>
     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
