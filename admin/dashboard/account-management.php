<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">
<?php include("top-navigation.html"); ?>
<?php include("modal-payment-fitur-ditolak.html"); ?>
      
<?php include("leftside.html"); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Account Management
        <small>search</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Advanced Elements</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    	<div class="row">
    		<div class="col-md-12">
    			<div class="box">
    				<form class="form-horizontal">
	                	<div class="box-body">
		                    <div class="form-group">
		                    	<label class="col-sm-2">Email</label>
		                    	<div class="col-sm-9"><input type="text" class="form-control" placeholder="Email"></div>
		                    </div>
		                    <div class="form-group">
		                    	<label class="col-sm-2">Username</label>
		                    	<div class="col-sm-9"><input type="text" class="form-control" placeholder="Username"></div>
		                    </div>
		                    <div class="form-group">
		                    	<label class="col-sm-2">Company Page</label>
		                    	<div class="col-sm-9"><input type="text" class="form-control" placeholder="Company Page"></div>
		                    	<div class="col-sm-1"><button type="submit" class="btn btn-default pull-right">Search</button></div>
		                    </div>
	                	</div>
	                </form>
	                <div class="box-body no-padding"> 
	                	<div class="col-sm-12">
	                		<table id="example2" class="table table-bordered table-hover" >
	                			<tr>
			                        <th class="col-md-1">ID</th>
			                        <th class="col-md-1">Username</th>
			                        <th class="col-md-2">Email</th>
			                        <th class="col-md-1">Nama</th>
			                        <th class="col-md-1">No Hp</th>
			                        <th class="col-md-3">Nama Perusahaan</th>
			                        <th class="col-md-2">Company Page</th>
			                        <th class="col-md-2">Action</th>
			                    </tr>
			                    <tr>
			                    	<td>1234</td>
			                    	<td>Abahnana</td>
			                    	<td>abahnana@email.com</td>
			                    	<td>Abah Nana</td>
			                    	<td>085678910009</td>
			                    	<td>CV. Setia Raja Ampat</td>
			                    	<td>http://abahnana.indonetwork.co.id/</td>
			                    	<td><a href="account-management-detail.php" class="btn btn-flat btn-danger">Detail</a></td>
			                    </tr>
			                    <tr>
			                    	<td>1234</td>
			                    	<td>Abahnana</td>
			                    	<td>abahnana@email.com</td>
			                    	<td>Abah Nana</td>
			                    	<td>085678910009</td>
			                    	<td>CV. Setia Raja Ampat</td>
			                    	<td>http://abahnana.indonetwork.co.id/</td>
			                    	<td><a href="account-management-detail.php" class="btn btn-flat btn-danger">Detail</a></td>
			                    </tr>
			                    <tr>
			                    	<td>1234</td>
			                    	<td>Abahnana</td>
			                    	<td>abahnana@email.com</td>
			                    	<td>Abah Nana</td>
			                    	<td>085678910009</td>
			                    	<td>CV. Setia Raja Ampat</td>
			                    	<td>http://abahnana.indonetwork.co.id/</td>
			                    	<td><a href="account-management-detail.php" class="btn btn-flat btn-danger">Detail</a></td>
			                    </tr>
			                    <tr>
			                    	<td>1234</td>
			                    	<td>Abahnana</td>
			                    	<td>abahnana@email.com</td>
			                    	<td>Abah Nana</td>
			                    	<td>085678910009</td>
			                    	<td>CV. Setia Raja Ampat</td>
			                    	<td>http://abahnana.indonetwork.co.id/</td>
			                    	<td><a href="account-management-detail.php" class="btn btn-flat btn-danger">Detail</a></td>
			                    </tr>
			                    <tr>
			                    	<td>1234</td>
			                    	<td>Abahnana</td>
			                    	<td>abahnana@email.com</td>
			                    	<td>Abah Nana</td>
			                    	<td>085678910009</td>
			                    	<td>CV. Setia Raja Ampat</td>
			                    	<td>http://abahnana.indonetwork.co.id/</td>
			                    	<td><a href="account-management-detail.php" class="btn btn-flat btn-danger">Detail</a></td>
			                    </tr>
			                    <tr>
			                    	<td>1234</td>
			                    	<td>Abahnana</td>
			                    	<td>abahnana@email.com</td>
			                    	<td>Abah Nana</td>
			                    	<td>085678910009</td>
			                    	<td>CV. Setia Raja Ampat</td>
			                    	<td>http://abahnana.indonetwork.co.id/</td>
			                    	<td><a href="account-management-detail.php" class="btn btn-flat btn-danger">Detail</a></td>
			                    </tr>
			                    <tr>
			                    	<td>1234</td>
			                    	<td>Abahnana</td>
			                    	<td>abahnana@email.com</td>
			                    	<td>Abah Nana</td>
			                    	<td>085678910009</td>
			                    	<td>CV. Setia Raja Ampat</td>
			                    	<td>http://abahnana.indonetwork.co.id/</td>
			                    	<td><a href="account-management-detail.php" class="btn btn-flat btn-danger">Detail</a></td>
			                    </tr>
	                		</table>
	                	</div>
	                </div>
	                <div>
	                	<div class="col-sm-5">
	                		<div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
	                	</div>
	                	<div class="col-sm-7">
	                		<div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
	                			<ul class="pagination">
	                				<li class="paginate_button previous disabled" id="example1_previous"><a href="#" aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a></li>
	                				<li class="paginate_button active"><a href="#" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a></li>
	                				<li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a></li>
	                				<li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" tabindex="0">3</a></li>
	                				<li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="4" tabindex="0">4</a></li>
	                				<li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="5" tabindex="0">5</a></li>
	                				<li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="6" tabindex="0">6</a></li>
	                				<li class="paginate_button next" id="example1_next"><a href="#" aria-controls="example1" data-dt-idx="7" tabindex="0">Next</a></li>
	                			</ul>
	                		</div>
	                	</div>
	                </div>
    			</div>
    		</div>
    	</div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>
<?php //include("general-script.html"); ?>

     <!-- Page script -->
         <script>
    $(function () {
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
      });
    });
    </script>

      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>