<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("modal-moderation.html"); ?>

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Metode Pembayaran
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-body">
                    <ul class="upgrade-member">
                      <li class="col-xs-12">
                        <div class="col-xs-12">
                          <ul class="progress-indicator">
                            <li class="completed">
                                <span class="stepbubble"></span>
                                1. Pilih Metode Pembayaran
                            </li>
                            <li>
                                <span class="stepbubble"></span>
                                2. Aktivasi Keanggotaan Terpilih
                            </li>
                            <li>
                                <span class="stepbubble"></span>
                                3. Konfirmasi Pembayaran
                            </li>
                          </ul>
                        </div>
                      </li>
                      <li class="col-xs-12">
                          <div class="col-xs-6 text-center">
                            <div class="bca">
                              <img src="../../assets/images/bca-icon.png" class="middle" align="middle"><br>
                              <label><input type="radio" name="transfer"> Pembayaran Transfer ke rekening BCA Rupiah</label>
                            </div>
                          </div>
                          <div class="col-xs-6 text-center">
                            <div class="mandiri">
                              <img src="../../assets/images/mandiri-icon.png" class="middle" align="middle"><br>
                              <label><input type="radio" name="transfer"> Pembayaran Transfer ke rekening Mandiri Rupiah</label>
                            </div>
                          </div>
                      </li>
                      <li class="col-xs-12">
                        <div class="text-center">
                          <a href="account-management-detail.php" class="btn btn-warning">Kembali</a>
                          <a href="upgrade-step2.php" class="btn btn-warning">Selanjutnya</a>
                        </div>
                      </li>
                    </ul>
                </div>
              </div>
              </div>
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>



     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
