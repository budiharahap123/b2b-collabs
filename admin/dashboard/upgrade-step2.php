<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("modal-moderation.html"); ?>

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Instruksi Pembayaran
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-body">
                    <ul class="upgrade-member">
                      <li class="col-xs-12">
                        <div class="col-xs-12">
                          <ul class="progress-indicator">
                            <li class="completed">
                                <span class="stepbubble"></span>
                                1. Pilih Metode Pembayaran
                            </li>
                            <li class="completed">
                                <span class="stepbubble"></span>
                                2. Aktivasi Keanggotaan Terpilih
                            </li>
                            <li>
                                <span class="stepbubble"></span>
                                3. Konfirmasi Pembayaran
                            </li>
                          </ul>
                        </div>
                      </li>
                      <li class="col-xs-12">
                        <p>Aktivasi keanggotaan Prioritas/Layanan SMS Indonetwork.<br>Untuk mengaktifkan Layanan anda, harap melakukan transaksi dengan informasi dibawah ini:</p>
                      </li>
                      <li class="col-xs-12">
                        <table class="table table-striped table-bordered">
                          <th colspan="2">Pembayaran dengan Transfer Bank (T/T)</th>
                          <tr>
                            <td class="col-sm-2">Nomor Rekening</td>
                            <td>0101-863963</td>
                          </tr>
                           <tr>
                            <td>Nama Rekening</td>
                            <td>PT Indonetwork Mitra Utama</td>
                          </tr>
                          <tr>
                            <td>Keanggotaan</td>
                            <td>Indonetwork Prioritas</td>
                          </tr>
                          <tr>
                            <td>Jumlah</td>
                            <td>Rp 1.700.000,-</td>
                          </tr>
                          <tr>
                            <td>Bank</td>
                            <td>BCA KCU veteran</td>
                          </tr>
                          <tr>
                            <td>Alamat</td>
                            <td>Jl Veteram 18-24, Surabaya Indonesia</td>
                          </tr>
                          <tr>
                            <td>Internet banking</td>
                            <td>BCA Internet Banking</td>
                          </tr>
                          <tr>
                            <td>Berita</td>
                            <td>User : Luks14Test <br>Perusahaan: CV Serba Indah</td>
                          </tr>
                        </table>
                      </li>
                      <li class="col-xs-12">
                        <div class="text-center">
                          <a href="upgrade-step1.php" class="btn btn-warning">Kembali</a>
                          <a href="upgrade-step3.php" class="btn btn-warning">Selanjutnya</a>
                        </div>
                      </li>
                    </ul>
                </div>
              </div>
              </div>
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>



     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
