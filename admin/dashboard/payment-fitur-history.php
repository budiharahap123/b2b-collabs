
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">
<?php include("top-navigation.html"); ?>
<?php include("modal-payment-fitur-ditolak.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Payment Top Up
            <small>search</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <form class="form-horizontal">
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2">Email</label>
                      <div class="col-sm-9"><input type="text" class="form-control" placeholder="Email"></div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2">Tanggal Dibuat</label>
                      <div class="col-sm-9">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                          </div>
                          <input type="text" class="form-control pull-right" id="reservationtime">
                        </div><!-- /.input group -->
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2">Status Pembayaran</label>
                      <div class="col-sm-9">
                        <select class="form-control">
                          <option>Semua</option>
                          <option>Baru</option>
                          <option>Diterima</option>
                          <option>Ditolak</option>
                        </select>
                      </div>
                      <div class="col-sm-1"><button type="submit" class="btn btn-default pull-right">Search</button></div>
                    </div>
                  </div>
                </form>
                <div class="box-body no-padding">               
                  <div class="col-sm-12">
                    <table id="example2" class="table table-bordered table-hover" >
                      <tr>
                        <th class="col-md-1">ID</th>
                        <th class="col-md-2">Tanggal <i class="fa fa-sort-amount-desc pull-right" aria-hidden="true"></i></th>
                        <th class="col-md-3">Email</th>
                        <th class="col-md-2">Jumlah</th>
                        <th class="col-md-1">Bank</th>
                        <th class="col-md-1">Status</th>
                        <th class="col-md-2">Link Bukti</th>
                        <th class="col-md-2">Action</th>
                      </tr>
                      <tr>
                        <td>123456</td>
                        <td>25/02/2016</td>
                        <td>nama.moderator@indonetwork.co.id</td>
                        <td>Rp 1.000.000</td>
                        <td>BCA</td>
                        <td>Baru</td>
                        <td>https://www.link.com/pict</td>
                        <td><a href="payment-fitur-detail.php" class="btn btn-flat btn-danger">Detail</a></td>
                      </tr>
                      <tr>
                        <td>123456</td>
                        <td>25/02/2016</td>
                        <td>nama.moderator@indonetwork.co.id</td>
                        <td>Rp 1.000.000</td>
                        <td>BCA</td>
                        <td>Baru</td>
                        <td>https://www.link.com/pict</td>
                        <td><a href="payment-fitur-detail.php" class="btn btn-flat btn-danger">Detail</a></td>
                      </tr>
                      <tr>
                        <td>123456</td>
                        <td>25/02/2016</td>
                        <td>nama.moderator@indonetwork.co.id</td>
                        <td>Rp 1.000.000</td>
                        <td>BCA</td>
                        <td>Baru</td>
                        <td>https://www.link.com/pict</td>
                        <td><a href="payment-fitur-detail.php" class="btn btn-flat btn-danger">Detail</a></td>
                      </tr>
                       <tr>
                        <td>123456</td>
                        <td>25/02/2016</td>
                        <td>nama.moderator@indonetwork.co.id</td>
                        <td>Rp 1.000.000</td>
                        <td>BCA</td>
                        <td>Diterima</td>
                        <td>https://www.link.com/pict</td>
                        <td><a class="btn btn-block btn-default" data-toggle="modal" data-target="#myModal">Detail</a></td>
                      </tr>
                      <tr>
                        <td>123456</td>
                        <td>25/02/2016</td>
                        <td>nama.moderator@indonetwork.co.id</td>
                        <td>Rp 1.000.000</td>
                        <td>BRI</td>
                        <td>Ditolak</td>
                        <td>https://www.link.com/pict</td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>123456</td>
                        <td>25/02/2016</td>
                        <td>nama.moderator@indonetwork.co.id</td>
                        <td>Rp 1.000.000</td>
                        <td>BCA</td>
                        <td>Diterima</td>
                        <td>https://www.link.com/pict</td>
                        <td><a class="btn btn-block btn-default" data-toggle="modal" data-target="#myModal">Detail</a></td>
                      </tr>
                      <tr>
                        <td>123456</td>
                        <td>25/02/2016</td>
                        <td>nama.moderator@indonetwork.co.id</td>
                        <td>Rp 1.000.000</td>
                        <td>BCA</td>
                        <td>Diterima</td>
                        <td>https://www.link.com/pict</td>
                        <td><a class="btn btn-block btn-default" data-toggle="modal" data-target="#myModal">Detail</a></td>
                      </tr>
                    </table>
                    <div class="row">
                      <div class="col-sm-5 pull-right">
                        <div class="dataTables_paginate paging_simple_numbers pull-left" id="example1_paginate"><ul class="pagination"><li class="paginate_button next" id="example1_next"><a href="#">First</a></li><li class="paginate_button previous disabled" id="example1_previous"><a href="#">Previous</a></li><li class="paginate_button next" id="example1_next"><a href="#">Next</a></li><li class="paginate_button next" id="example1_next"><a href="#">Last</a></li></ul>
                        </div>
                        <span class="col-sm-4 pagination dataTables_info pull-right"> 1 / 123456 pages</span>
                      </div>
                    </div>
                  </div>
                </div><!-- /.box-body -->
              </div>
            </div><!-- /.col (right) -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>
<?php //include("general-script.html"); ?>

     <!-- Page script -->
         <script>
    $(function () {
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
      });
    });
    </script>

      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
