
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">
      
<?php include("modal-supervise.html"); ?>

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Pre Moderation Supervise
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="row">

          
              <div class="col-md-12">
              <div class="box box-danger">
              
              <div class="box-header with-border">
              <h3 class="box-title">Pilih Moderasi</h3>
              </div>
              <form action="/premoderation/index" method="post"><div class="box-body">
              <div class="row">
              <div class="col-md-12">
              <table class="table table-bordered">
              <tbody>
              <tr>
              <td><b>Total Product Pending Supervise</b></td>
              <td>
              <label>
              376
              </label>
              </td>
              </tr>
              </tbody>
              </table>
              </div>
              
            

            
              <div class="col-md-12">
              <div>
              <button class="btn btn-primary" type="submit">Cek Produk Sekarang</button>
              </div>
              </div>

              
              </div><!-- /.row -->
              </div><!-- /.box-body -->
              </form>
              </div>
              </div>

          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>

     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
