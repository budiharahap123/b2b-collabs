

<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Category Change
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">


          <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">

              <div class="box margintop">

                <table class="table table-striped text-center">
                <tr>
                  <td class="txtcenter">Pilih Kategori Utama</td>
                </tr>
                <tr>
                  <td class="txtcenter">Welcome, Super Admin</td>
                </tr>
                <tr>
                  <td>
                    <select class="form-control">
                      <option>Teknologi & Komunikasi</option>
                      <option>Kesehatan & Kecantikan</option>
                      <option>Media, Musik 7 Buku</option>
                      <option>Pertanian & Produk Pangan</option>
                      <option>Peralatan Kantor</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td class="text-center"><a href="change-category-list.php"><button type="button" class="btn btn-primary btn-sm" type="button" >Submit</button></a></td>
                </tr>
              </tbody></table>

              </div>
            
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>

     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
