
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("modal-moderation.html"); ?>

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Moderasi Produk
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-12">

          <!-- SELECT2 EXAMPLE -->
          <div class="box">

            <div class="box-header with-border">
              <h3 class="box-title">Form Pencarian</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">

                  <div class="input-group">
                  <input type="text" class="form-control">
                <div class="input-group-btn">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Nama
                    &nbsp;&nbsp;<span class="fa fa-caret-down"></span></button>
                  <ul class="dropdown-menu">
                    <li><a href="#">Nama</a></li>
                    <li><a href="#">Username</a></li>
                  </ul>
                  <button type="button" class="btn btn-primary">Cari</button>
                </div>
                <!-- /btn-group -->
                
              </div>

                </div><!-- /.col -->
                
              </div><!-- /.row -->
            </div><!-- /.box-body -->

            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-12"><div class="dataTables_length" id="example1_length"><label>Tampilkan&nbsp;&nbsp;<select name="example1_length" aria-controls="example1" class="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select>&nbsp;&nbsp;data</label></div></div></div></div><div class="row"><div class="col-sm-12"><div class="table-responsive"><table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead>
                <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="UserID: activate to sort column descending">ID Request</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="User Email: activate to sort column ascending">Username</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Nama</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Alamat</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Status</th><th tabindex="0" aria-controls="example1" rowspan="1" >Action</th>
                </tr>
                </tr>
                </thead>
                <tbody>
                
                <tr role="row" class="odd">
                  <td class="sorting_1">1</td>
                  <td><a href="">Sepeda Roda Tiga</a></td><td><small>Soeharto</small></td><td><small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small></td><td>Belum Dikirim</td>
                  <td><button type="button" class="btn btn-primary btn-xs" onclick="location.href='./verification-print.php'" >Print</button></td>
                </tr><tr role="row" class="even">
                  <td class="sorting_1">2</td>

                  <td><a href="">Mouse Komputer</a></td><td><small>Basuki Chandra</small></td><td><small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small></td><td>Belum Dikirim</td>
                  <td><button type="button" class="btn btn-primary btn-xs" onclick="location.href='./verification-print.php'" >Print</button>
                      </td>
                </tr><tr role="row" class="odd">
                  <td class="sorting_1">3</td>
                  <td><a href="">Baju Branding</a></td><td><small>Si Beye</small></td><td><small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small></td><td>Sudah Dikirim</td>
                  <td><button type="button" class="btn btn-primary btn-xs" onclick="location.href='./verification-print.php'" >Print</button>
                      </td>
                </tr><tr role="row" class="even">
                  <td class="sorting_1">4</td>

                  <td><a href="">Hehehe</a></td><td><small>Bambang Pamungkas</small></td><td><small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small></td><td>Sudah Dikirim</td>
                  <td><button type="button" class="btn btn-primary btn-xs" onclick="location.href='./verification-print.php'" >Print</button>
                      </td>
                </tr><tr role="row" class="odd">
                  <td class="sorting_1">5</td>
                  <td><a href="">Celana Modis</a></td><td><small>Anas Urbaningrum</small></td><td><small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small></td><td>Aktif</td>
                  <td><button type="button" class="btn btn-primary btn-xs" onclick="location.href='./verification-print.php'" >Print</button>
                      </td>
                </tr><tr role="row" class="even">
                  <td class="sorting_1">6</td>

                  <td><a href="">Coklat Batangan</a></td><td><small>Chairil Tanjung</small></td><td><small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small></td><td>Aktif</td>
                  <td><button type="button" class="btn btn-primary btn-xs" onclick="location.href='./verification-print.php'" >Print</button>
                      </td>
                </tr><tr role="row" class="odd">
                  <td class="sorting_1">7</td>
                  <td><a href="">Baju Branding</a></td><td><small>Bruce Banner</small></td><td><small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small></td><td>Sudah Dikirim</td>
                  <td><button type="button" class="btn btn-primary btn-xs" onclick="location.href='./verification-print.php'" >Print</button>
                      </td>
                </tr><tr role="row" class="even">
                  <td class="sorting_1">8</td>

                  <td><a href="">Klik mapan</a></td><td><small>Peter Parker</small></td><td><small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small></td><td>Gagal</td>
                  <td><button type="button" class="btn btn-primary btn-xs" onclick="location.href='./verification-print.php'" >Print</button>
                      </td>
                </tr><tr role="row" class="odd">
                  <td class="sorting_1">9</td>
                  <td><a href="">Jual Daging Impor</a></td><td><small>Wade Wilson</small></td><td><small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small></td><td>Gagal</td>
                  <td><button type="button" class="btn btn-primary btn-xs" onclick="location.href='./verification-print.php'" >Print</button>
                      </td>
                </tr><tr role="row" class="even">
                  <td class="sorting_1">10</td>

                  <td><a href="">Jualan Halalan</a></td><td><small>Clark Kent</small></td><td><small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small></td><td>Aktif</td>
                  <td><button type="button" class="btn btn-primary btn-xs" onclick="location.href='./verification-print.php'" >Print</button>
                      </td>
                </tr></tbody>
                <tfoot>
                <tr><th rowspan="1" colspan="1">ID Request</th><th rowspan="1" colspan="1">Username</th><th rowspan="1" colspan="1">Nama</th><th rowspan="1" colspan="1">Alamat</th><th rowspan="1" colspan="1">Status</th><th rowspan="1" colspan="1">Action</th><th rowspan="1" colspan="1"> </th></tr>
                </tfoot>
              </table></div></div></div><div class="row"><div class="col-sm-5"><div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div><div class="col-sm-7"><div class="dataTables_paginate paging_simple_numbers" id="example1_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="example1_previous"><a href="#" aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a></li><li class="paginate_button active"><a href="#" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" tabindex="0">3</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="4" tabindex="0">4</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="5" tabindex="0">5</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="6" tabindex="0">6</a></li><li class="paginate_button next" id="example1_next"><a href="#" aria-controls="example1" data-dt-idx="7" tabindex="0">Next</a></li></ul></div></div></div></div>
            </div>
            <!-- /.box-body -->
          </div>
            
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>



     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
