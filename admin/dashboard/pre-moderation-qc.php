
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">
      
<?php include("modal-supervise.html"); ?>

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Pilih Moderasi
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="row">

          
              <div class="col-md-12">
              <div class="box box-danger">
              
              <div class="box-header with-border">
              <h3 class="box-title">Pilih Moderasi</h3>
              </div>
              <form action="/premoderation/index" method="post"><div class="box-body">
              <div class="row">
              <div class="col-md-12">
              <table class="table table-bordered">
              <tbody>
              <tr>
              <td>Status</td>
              <td>
              <label>
              <input type="radio" id="status" name="status" value="0" checked="checked"> Pending
              </label>
              </td>
              </tr>

              <tr>
              <td>Email</td>
              <td>
              <div class="form-group">
              <input type="email" class="form-control" placeholder="Find email address">
              <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <small> E-mail tidak ditemukan </small>
                  </div>
              </div>
              </td>
              </tr>

              <tr>
              <td>Title Product</td>
              <td>
              <div class="form-group">
              <input type="text" class="form-control" placeholder="Find title product">
              <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <small> Title Produk tidak ditemukan </small>
                  </div>
              </div>
              </td>
              </tr>
              
              </td>
              </tr>
              <tr>
              <td>Total Lock</td>
              <td>
              <table class="full">
              <tbody>
              <tr>
              <td>
              <table class="full">
              <tbody>
              <tr>
              <td><label><input type="radio" id="limit" name="limit" value="50" checked="checked"> 50</label></td>
              </tr>
              <tr>
              <td><label><input type="radio" id="limit" name="limit" value="100"> 100</label></td>
              </tr>
              <tr>
              <td><label><input type="radio" id="limit" name="limit" value="150"> 150</label></td>
              </tr>
              <tr>
              <td><label><input type="radio" id="limit" name="limit" value="200"> 200</label></td>
              </tr>
              <tr>
              <td colspan="4">
              </td>
              </tr>
              </tbody>
              </table>
              </td>
              </tr>
              </tbody>
              </table>
              </td>
              </tr>
              </tbody>
              </table>
              </div><!-- /.col -->
              <div class="col-md-12">
              <div class="text-center">
              <button class="btn btn-primary" type="submit">Lock Now</button>
              </div>
              </div>
              
              </div><!-- /.row -->
              </div><!-- /.box-body -->
              </form>
              </div>
              </div>

          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>

     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
