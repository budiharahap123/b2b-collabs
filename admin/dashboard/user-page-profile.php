
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Profil Users/Members
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">


          <div class="row">
            <div class="col-md-8">

              <div class="box">

                <table class="table table-striped">
                <tbody>
                <tr>
                  <td>UserID</td>
                  <td>123456</td>
                </tr>
                <tr>
                  <td>Username</td>
                  <td>Budiadiliansyah</td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td><a href="">budi.pardomuan@mataharibiz.com</a></td>
                </tr>
                <tr>
                  <td>Membership Type</td>
                  <td>Prioritas</td>
                </tr>
                <tr>
                  <td>Status</td>
                  <td>Aktif</td>
                </tr>
                <tr>
                  <td>Nama</td>
                  <td>Budiadiliansyah</td>
                </tr>
                <tr>
                  <td>Nomor HP</td>
                  <td>0856 123 456</td>
                </tr>
                <tr>
                  <td>Nomor HP SMS</td>
                  <td>0856 123 456</td>
                </tr>
                <tr>
                  <td>Nama Perusahaan</td>
                  <td>PT Handal Jaya Berjaya</td>
                </tr>
                <tr>
                  <td>Company Page</td>
                  <td><a href="">http://handaljaya.indonetwork.co.id</a></td>
                </tr>
                <tr>
                  <td>Alamat Perusahaan</td>
                  <td>Jalan Veteran No. 16, Medan Merdeka Utara 123456. Jakarta Selatan - DKI Jakarta</td>
                </tr>
                <tr>
                  <td> </td>
                  <td><button type="button" class="btn btn-primary btn-sm" onclick="window.location.href='user-profile-form.php'">Edit User</button> <button type="button" class="btn btn-danger btn-sm" onclick="window.location.href='deactivate-user.php'">Nonaktifkan User</button> <button type="button" class="btn btn-primary btn-sm" onclick="window.location.href='free-membership.php'">Free Membership</button></td>
                </tr>
              </tbody></table>



              </div>
            
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>



     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
