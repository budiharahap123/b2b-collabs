
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Moderasi Produk
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">


          <div class="row">
            <div class="col-md-12">

            <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Moderation History Search Form</h3>
                </div><!-- /.box-header -->

              <form class="form-horizontal">

                <div class="box-body">

                    <div class="form-group">
                    <label class="col-sm-2">Product ID</label>
                    <div class="col-sm-9"><input type="text" class="form-control" placeholder="produk ID"></div>
                    </div>

                    <div class="form-group">
                    <label class="col-sm-2">Date Range</label>
                    <div class="col-sm-9"><input type="text" class="form-control" placeholder="date range"></div>
                    </div>

                    <div class="form-group">
                    <label class="col-sm-2">Status</label>
                    <div class="col-sm-9"><select class="form-control">
                        <option>Approved</option>
                        <option>Rejected</option>
                      </select></div>
                    </div>

                    <div class="form-group">
                    <label class="col-sm-2">Moderator</label>
                    <div class="col-sm-9"><input type="text" class="form-control" placeholder="Moderator"></div>
                    <div class="col-sm-1"><button type="submit" class="btn btn-default pull-right">Search</button></div>
                    </div>

                </div>

                </form>

                <div class="box-header with-border box-footer">
                <h3 class="box-title">List Moderation History</h3>
            </div>

            <div class="box-body no-padding">

                    <table class="table table-bordered table-striped noselect">
                      <tbody>
                        <tr>
                          <th>ID</th>
                          <th>Title Product</th>
                          <th>Status</th>
                          <th>Moderator</th>
                          <th>Action Date</th>
                        </tr>
                      <tr>
                        <td>123456</td>
                        <td>Software OS</td>
                        <td><span class="label label-success">Approved</span></td>
                        <td>nama.moderator@indonetwork.co.id</td>
                        <td>25/02/2016</td>
                      </tr>
                      <tr>
                        <td>123456</td>
                        <td>Sex Toys</td>
                        <td><span class="label label-danger">Rejected</span></td>
                        <td>nama.moderator@indonetwork.co.id</td>
                        <td>24/02/2016</td>
                      </tr>
                      <tr>
                        <td>123456</td>
                        <td>Jam Tangan OS</td>
                        <td><span class="label label-success">Approved</span></td>
                        <td>nama.moderator@indonetwork.co.id</td>
                        <td>24/02/2016</td>
                      </tr>
                      <tr>
                        <td>123456</td>
                        <td>Kipas Angin</td>
                        <td><span class="label label-success">Approved</span></td>
                        <td>nama.moderator@indonetwork.co.id</td>
                        <td>24/02/2016</td>
                      </tr>
                      <tr>
                        <td>123456</td>
                        <td>Gendam via Bluetooth</td>
                        <td><span class="label label-danger">Rejected</span></td>
                        <td>nama.moderator@indonetwork.co.id</td>
                        <td>22/02/2016</td>
                      </tr>
                    </tbody>
                  </table><br />

            <div class="col-lg-6"><a href="" class="btn btn-default">Download CSV</a></div>
            <div class="col-lg-6 text-right">

              <ul class="pagination"> <li class="disabled"><a href="#" aria-label="Previous">First</a></li> <li class="active"><a href="#">Previous</a></li> <li><a href="#">Next</a></li> <li><a href="#" aria-label="Next">Last</a></li> </ul><br /><br />
            </div>
            </div><!-- /.box-body -->

            </div>

          </div>
          </div>
            
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>

     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
