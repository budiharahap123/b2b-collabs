
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Banned Keywords
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">


          <div class="row">

            <div class="col-md-8">

              <div class="input-group">
                <input type="text" class="form-control">
                <span class="input-group-btn">
                  <button class="btn btn-primary" type="button">+ Tambah Keyword</button>
                </span>
              </div>

              <div class="clear"></div><br />

            <div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                    Maaf, keyword sudah ada.
            </div>

            </div><!-- /.col (right) -->

            <div class="clear"></div><br /><br />

            <div class="col-md-12 keywords">

              <div class="box small-box box-danger bg-red">
                <div class="box-header" >
                  Sex Toys
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
              </div>

              <div class="box small-box box-danger bg-red">
                <div class="box-header" >
                  Pesugihan
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
              </div>

              <div class="box small-box box-danger bg-red">
                <div class="box-header" >
                  Ilmu Pelet dan Gendam
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
              </div>

              <div class="box small-box box-danger bg-red">
                <div class="box-header" >
                  Female Domination
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
              </div>

            </div><!-- /.col (right) -->

          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>



     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
