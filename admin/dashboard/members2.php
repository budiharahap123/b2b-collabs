
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Profil Membership
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">


          <div class="row">
          <div class="col-md-12">
                <div id="exTab2" class="col-md-12"> 
                  <ul class="nav nav-tabs">
                    <li class="active">
                      <a  href="#1" data-toggle="tab">Member Detail</a>
                    </li>
                    <li><a href="#2" data-toggle="tab">Member Product</a>
                    </li>
                  </ul>
                    <div class="box">
                      <div class="tab-content ">
                        <br>
                        <div class="tab-pane active" id="1">
                          <div class="col-md-12">
                            <table class="table table-striped">
                              <tr>
                                <td class="col-md-4">ID</td>
                                <td>123456</td>
                              </tr>
                              <tr>
                                <td>Username</td>
                                <td>Budiadiliansyah</td>
                              </tr>
                              <tr>
                                <td>Email</td>
                                <td><a href="">budi.pardomuan@mataharibiz.com</a></td>
                              </tr>
                              <tr>
                                <td>Membership Type</td>
                                <td>Prioritas</td>
                              </tr>
                              <tr>
                                <td>Membership Expire</td>
                                <td>20/10/2016</td>
                              </tr>
                              <tr>
                                <td>Status</td>
                                <td>Nonaktif</td>
                              </tr>
                              <tr>
                                <td>Nama</td>
                                <td>Budiadiliansyah</td>
                              </tr>
                              <tr>
                                <td>No. Telepon</td>
                                <td>+62123456789</td>
                              </tr>
                              <tr>
                                <td>No. HP</td>
                                <td>+62123456789</td>
                              </tr>
                              <tr>
                                <td>Nama Perushaan</td>
                                <td>handaljaya</td>
                              </tr>
                              <tr>
                                <td>Company Page</td>
                                <td><a href="">http://handaljaya.indonetwork.co.id</a></td>
                              </tr>
                              <tr>
                                <td>Alamat</td>
                                <td>20/10/2016</td>
                              </tr>
                              <tr>
                                <td><button type="button" class="btn btn-primary btn-sm" >Edit</button>   <button type="button" class="btn btn-danger btn-sm">Activation</button> <button type="button" class="btn btn-primary btn-sm" onclick="window.location.href='#'">Free Membership</button></td>
                                <td class="text-right"> <button type="button" class="btn btn-default btn-sm" onclick="location.href='#'">Back</button></td>
                              </tr>
                            </table>
                        </div>
                        </div>
                        <div class="tab-pane" id="2">
                          <div class="box-body no-padding">               
                          <div class="col-sm-12">
                              <table id="example2" class="table table-bordered table-hover" >
                              <tbody>
                                  <tr>
                                    <th class="col-md-1">ID</th>
                                    <th class="col-md-2">Tanggal dibuat<i class="fa fa-sort-amount-desc pull-right" aria-hidden="true"></i></th>
                                    <th class="col-md-2">Title Product</th>
                                    <th class="col-md-5">Description</th>
                                    <th class="col-md-1">Category</th>
                                    <th class="col-md-1">Status</th>
                                    <th class="col-md-1">Action</th>
                                  </tr>
                                  <tr>
                                    <td>123456</td>
                                    <td>25/02/2016</td>
                                    <td>Controller XBOX 360</td>
                                    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.</td>
                                    <td>Video Game</td>
                                    <td>Aktif</td>
                                     <td><a href="#" class="btn btn-flat btn-default">View</a></td>
                                  </tr>
                                  <tr>
                                    <td>123456</td>
                                    <td>25/02/2016</td>
                                    <td>Controller XBOX 360</td>
                                    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.</td>
                                    <td>Video Game</td>
                                    <td>Aktif</td>
                                     <td><a href="#" class="btn btn-flat btn-default">View</a></td>
                                  </tr>
                                  <tr>
                                    <td>123456</td>
                                    <td>25/02/2016</td>
                                    <td>Controller XBOX 360</td>
                                    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.</td>
                                    <td>Video Game</td>
                                    <td>Aktif</td>
                                     <td><a href="#" class="btn btn-flat btn-default">View</a></td>
                                  </tr>
                                  <tr>
                                    <td>123456</td>
                                    <td>25/02/2016</td>
                                    <td>Controller XBOX 360</td>
                                    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.</td>
                                    <td>Video Game</td>
                                    <td>Aktif</td>
                                     <td><a href="#" class="btn btn-flat btn-default">View</a></td>
                                  </tr>
                                  <tr>
                                    <td>123456</td>
                                    <td>25/02/2016</td>
                                    <td>Controller XBOX 360</td>
                                    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.</td>
                                    <td>Video Game</td>
                                    <td>Aktif</td>
                                     <td><a href="#" class="btn btn-flat btn-default">View</a></td>
                                  </tr>
                                  <tr>
                                    <td>123456</td>
                                    <td>25/02/2016</td>
                                    <td>Controller XBOX 360</td>
                                    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.</td>
                                    <td>Video Game</td>
                                    <td>Aktif</td>
                                     <td><a href="#" class="btn btn-flat btn-default">View</a></td>
                                  </tr>
                                  <tr>
                                    <td>123456</td>
                                    <td>25/02/2016</td>
                                    <td>Controller XBOX 360</td>
                                    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.</td>
                                    <td>Video Game</td>
                                    <td>Aktif</td>
                                     <td><a href="#" class="btn btn-flat btn-default">View</a></td>
                                  </tr>
                                  <tr>
                                    <td>123456</td>
                                    <td>25/02/2016</td>
                                    <td>Controller XBOX 360</td>
                                    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.</td>
                                    <td>Video Game</td>
                                    <td>Aktif</td>
                                     <td><a href="#" class="btn btn-flat btn-default">View</a></td>
                                  </tr>
                                  <tr>
                                    <td>123456</td>
                                    <td>25/02/2016</td>
                                    <td>Controller XBOX 360</td>
                                    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.</td>
                                    <td>Video Game</td>
                                    <td>Aktif</td>
                                     <td><a href="#" class="btn btn-flat btn-default">View</a></td>
                                  </tr>
                                  <tr>
                                    <td>123456</td>
                                    <td>25/02/2016</td>
                                    <td>Controller XBOX 360</td>
                                    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.</td>
                                    <td>Video Game</td>
                                    <td>Aktif</td>
                                     <td><a href="#" class="btn btn-flat btn-default">View</a></td>
                                  </tr>
                              </tbody>
                              </table>
                              <div class="row">
                                <div class="col-sm-6"><button type="button" class="btn btn-primary btn-sm" onclick="window.location.href='add-product-members.php'">Add New Product</button></div>
                                <div class="col-sm-6 text-right"><button type="button" class="btn btn-default btn-sm" onclick="location.href='payment-fitur-history.php'">Back</button></div>
                              </div>
                              <br>
                          </div>
                      </div><!-- /.box-body -->
                        </div>
                        <div class="tab-pane" id="3">
                          <h3>add clearfix to tab-content (see the css)</h3>
                        </div>
                      </div>
                    </div>
                </div>
          </div>
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>



     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
