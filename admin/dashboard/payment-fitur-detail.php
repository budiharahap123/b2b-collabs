
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("modal.html"); ?>

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Payment Fitur
            <small>Detail</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">


          <div class="row">
            <div class="col-md-8">

              <div class="box">
                <table class="table table-striped">
                <tbody>
                 <tr>
                  <td class="col-md-3">Payment Detail</td>
                  <td></td>
                </tr>
                <tr>
                  <td>ID</td>
                  <td>123456</td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td>email.email@gmail.com</td>
                </tr>
                <tr>
                  <td>Nama Perusahaan</td>
                  <td><a href="">PT. Perkasa Muda</a></td>
                </tr>
                <tr>
                  <td>Company Page</td>
                  <td>jayakusir.indonetwork.co.id</td>
                </tr>
                <tr>
                  <td>Nama Pengirim</td>
                  <td>Basara Yuda</td>
                </tr>
                <tr>
                  <td>Bank Pengirim</td>
                  <td>Mandiri</td>
                </tr>
                <tr>
                  <td>Jumlah Topup</td>
                  <td>Rp. 10.000.000</td>
                </tr>
                <tr>
                  <td>Status Pembayaran</td>
                  <td>Diterima</td>
                </tr>
                <tr>
                  <td>Bukti Transaksi</td>
                  <td>receipt_123</td>
                </tr>
                <tr>
                  <td>Keterangan Tambahan</td>
                  <td>Sudah saya transfer ya</td>
                </tr>
                <tr>
                  <td> </td>
                  <td class="text-right"><button type="button" class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#myModal">Verifikasi</button>   <button type="button" class="btn btn-danger btn-sm" >Tolak</button>   <button type="button" class="btn btn-default btn-sm" onclick="location.href='payment-fitur-history.php'">Back</button></td>
                </tr>
              </tbody></table>

              </div>
            
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>



     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
