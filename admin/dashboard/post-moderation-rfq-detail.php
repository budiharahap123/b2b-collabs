<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("modal-moderation-rfq.html"); ?>

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Post Moderation RFQ Detail
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">


          <div class="row">
            <div class="col-md-12">

              <div class="box">

                <table class="table table-striped">
                <tbody>
                <tr>
                  <td class="col-md-2">Produk ID</td>
                  <td>123456</td>
                </tr>
                 <tr>
                  <td>Tangaal dibuat</td>
                  <td>04/10/2016 14:12:33</td>
                </tr>
                <tr>
                  <td>Username</td>
                  <td>WIRATAMA2</td>
                </tr>
                <tr>
                  <td>Nama Perushaan</td>
                  <td>PT. Wiratama Mitra Abadi</td>
                </tr>
                 <tr>
                  <td>Nama Produk</td>
                  <td>Housing Sibas Connector HA.3.SG-LB.11</td>
                </tr>
                <tr>
                  <td>Harga</td>
                  <td>265.000</td>
                </tr>
                <tr>
                  <td>Link Permintaan</td>
                  <td><a href="http://handaljaya.indonetwork.co.id/detail/0000/baju-branding/">http://handaljaya.indonetwork.co.id/detail/0000/baju-branding/</a></td>
                </tr>
                <tr>
                <td>Deskripsi Permintaan</td>
                  <td>
                    <p>
                              <center> <b>Detail Blender Waring LB 20 G &amp; LB 20 S</b></center>

                      Model: LB 20 G &amp; LB 20 S<br>
                      Pembuatan: USA<br>
                      Dimensi: 0 cm x 0 cm x 0 cm<br>
                      Berat: 0 kg<br>
                      Produk Katalog: N / A<br>
                      Instruksi manual: N / A<br>
                      Spesifikasi Teknis: N / A<br>
                      Produk Atribut:<br>
                      Deskripsi Singkat :<br>
                      Blender laboratorium 1 liter<br>
                      Keterangan:<br>
                      Blendor, Cap. 1000 ml, kecepatan variabel 0-20,000 rpm, menyesuaikan untuk mencampur, mengaduk, pencampuran dan homogenisasi, 0,4 Hp, 220 V, 50 Hz. Dengan jenis sebagai berikut:
                      - Model LB-20S, Sertakan wadah stainless steel dengan pegangan dan dua potong vinyl tutup
                      - Model LB-20G. Termasuk wadah kaca dengan pegangan dan dua potong vinyl tutup
                    </p>
                  </td>
                </tr>
                <tr>
                  <td>Status</td>
                  <td>ACTIVE</td>
                </tr>
                <tr>
                  <td> </td>
                  <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Nonaktifkan Permintaan Ini</button></td>
                </tr>
              </tbody></table>

              </div>
            
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>



     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
