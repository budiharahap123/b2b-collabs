<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("modal-moderation.html"); ?>

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            List Produk Detail
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">


          <div class="row">
            <div class="col-md-9">

              <div class="box">

                <table class="table table-striped">
                <tbody>
                <tr>
                  <td class="col-sm-2">ID</td>
                  <td>123456</td>
                </tr>
                 <tr>
                  <td>Tanggal Dibuat</td>
                  <td>10/01/2016</td>
                </tr>
                <tr class="product-tab">
                  <td>Foto Produk</td>
                  <td>
                    <ul class="foto-produk input_fields_wrap">
                      <?php
                  for ($i=1; $i<=8; $i++) {
                    echo '<li><label><div class="imgbox"><img src="../../assets/data/foto'.$i.'.jpg"><input type="file" value="'.$i.'"></label></div></li>';
                  }
                  ?>                                               
                    </ul>
                  </td>
                </tr>
                <tr>
                  <td>Nama Produk</td>
                  <td>Sweet Almond Oil</td>
                </tr>
                <tr>
                  <td>Kategori</td>
                  <td>Agraris</td>
                </tr>
                <tr>
                  <td>Sub Kategori</td>
                  <td>Sub Agraris</td>
                </tr>
                <tr>
                  <td>Minimum Pesanan</td>
                  <td>10 Biji</td>
                </tr>
                <tr>
                  <td>Harga</td>
                  <td>265.000</td>
                </tr>
                <tr>
                  <td>Deskripsi Produk</td>
                  <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</td>
                </tr>
                <tr>
                  <td> </td>
                  <td><a href="acc-manage-listproduk-editing.php" class="btn btn-primary btn-sm" >Edit</a> <a href="account-management-detail.php" class="btn btn-default btn-sm" >Kembali</a></td>
                </tr>
              </tbody></table>

              </div>
            
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>



     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
