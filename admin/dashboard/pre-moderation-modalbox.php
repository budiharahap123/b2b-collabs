
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("modal-supervise.html"); ?>

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Moderasi Produk
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-12">

          <!-- SELECT2 EXAMPLE -->
          <div class="box">

            <div class="box-header with-border">
              <h3 class="box-title">List Moderasi</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="row">

                <div class="col-md-12">
                  <div class="row pre listingdata">
                    <div class="col-md-2"><div class="thumbnail"><div class="productdataimg"><a href=""><img class="block" src="../../assets/images/product-01b.jpg" border="0" alt=""></a></div></div></div>
                    <div class="col-md-2"><div class="thumbnail"><div class="productdataimg"><a href=""><img class="block" src="../../assets/images/product-01b.jpg" border="0" alt=""></a></div></div></div>
                    <div class="col-md-2"><div class="thumbnail"><div class="productdataimg"><a href=""><img class="block" src="../../assets/images/product-01b.jpg" border="0" alt=""></a></div></div></div>
                    <div class="col-md-2"><div class="thumbnail"><div class="productdataimg"><a href=""><img class="block" src="../../assets/images/product-01b.jpg" border="0" alt=""></a></div></div></div>
                    <div class="col-md-2"><div class="thumbnail"><div class="productdataimg"><a href=""><img class="block" src="../../assets/images/product-01b.jpg" border="0" alt=""></a></div></div></div>
                    <div class="col-md-2"><div class="thumbnail"><div class="productdataimg"><a href=""><img class="block" src="../../assets/images/product-01b.jpg" border="0" alt=""></a></div></div></div>
                    <div class="col-md-2"><div class="thumbnail"><div class="productdataimg"><a href=""><img class="block" src="../../assets/images/product-01b.jpg" border="0" alt=""></a></div></div></div>
                    <div class="col-md-2"><div class="thumbnail"><div class="productdataimg"><a href=""><img class="block" src="../../assets/images/product-01b.jpg" border="0" alt=""></a></div></div></div>
                  </div>
                </div>  
                <div class="col-md-12">
                  <div class="row pre">
                    <div class="col-md-2"><h4 class="text-center">A</h4></div>
                    <div class="col-md-2"><h4 class="text-center">S</h4></div>
                    <div class="col-md-2"><h4 class="text-center">D</h4></div>
                    <div class="col-md-2"><h4 class="text-center">F</h4></div>
                    <div class="col-md-2"><h4 class="text-center">Z</h4></div>
                    <div class="col-md-2"><h4 class="text-center">X</h4></div>
                    <div class="col-md-2"><h4 class="text-center">C</h4></div>
                    <div class="col-md-2"><h4 class="text-center">V</h4></div>
                  </div>
                </div>

                <div class="col-md-12"></div><br />
                              
                <div class="col-md-9">
                  <div class="row">




                    <div class="col-md-12">

                    <table class="table table-bordered">
                      <tbody>
                      <tr>
                        <td>Title Product</td>
                        <td>OS software</td>
                      </tr>
                      <tr>
                        <td>Deskripsi Product (ID)</td>
                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</td>
                      </tr>
                      <tr>
                        <td>Deskripsi Product (ENG)</td>
                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</td>
                      </tr>
                      <tr>
                        <td>Kategori Level I</td>
                        <td>Komputer</td>
                      </tr>
                      <tr>
                        <td>Kategori Level II</td>
                        <td>Operating System</td>
                      </tr>
                      <tr>
                        <td>Harga</td>
                        <td>Rp 1.000.000 hingga Rp 5.000.000</td>
                      </tr>
                      <tr>
                        <td>Minimum Pembelian</td>
                        <td>1</td>
                      </tr>
                    </tbody>
                  </table>

                  </div>

                  </div>
                </div><!-- /.col -->

                <div class="col-md-3">
                  <div class="well">
                  <ol class="listlegend">
                    <li class="red">Gambar produk mengandung watermark</li>
                    <li class="red">Gambar produk mengandung prohibited content</li>
                    <li class="red">Titel produk mengandung prohibited content</li>
                    <li class="red">Deskripsi produk mengandung prohibited content</li>
                    <li class="red">Harga terlalu mahal/murah</li>
                    <li>Titel produk tidak relevan/typo harus di edit</li>
                    <li>Kategori tidak sesuai</li>
                  </ol><br />
                  <div class="text-center"><span class="spacebar">Space Bar</span> = Approve</div>
                  </div>
 
                </div><!-- /.col -->


              </div><!-- /.row -->
            </div><!-- /.box-body -->

          </div>
            
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>

<script type="text/javascript">
    $(window).load(function(){
        $('#myModal').modal('show');
    });
</script>
     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
