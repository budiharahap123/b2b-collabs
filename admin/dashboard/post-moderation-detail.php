
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("modal-moderation.html"); ?>

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Post Moderation
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">


          <div class="row">
            <div class="col-md-8">

              <div class="box">

                <table class="table table-striped">
                <tbody>
                <tr>
                  <td>Produk ID</td>
                  <td>123456</td>
                </tr>
                <tr>
                  <td>Tanggal dibuat</td>
                  <td>10/01/2016</td>
                </tr>
                <tr>
                  <td>Username</td>
                  <td>Budiadiliansyah</td>
                </tr>
                <tr>
                  <td>Nama Perusahaan</td>
                  <td>PT Handal Jaya Berjaya</td>
                </tr>
                <tr>
                  <td>Company Page</td>
                  <td><a href="">http://handaljaya.indonetwork.co.id</a></td>
                </tr>
                <tr>
                  <td>Title Produk</td>
                  <td>Baju Branding</td>
                </tr> 
                <tr>
                  <td>Link Produk</td>
                  <td><a href="">http://handaljaya.indonetwork.co.id/detail/0000/baju-branding/</a></td>
                </tr>
                <tr>
                  <td>Kategori</td>
                  <td>
                    <div class="row">
                      <div class="col-sm-10">
                        <select class="form-control" disabled="">
                        <option>Teknologi &amp; Komunikasi</option>
                        <option>Kesehatan &amp; Kecantikan</option>
                        <option>Media, Musik 7 Buku</option>
                        <option>Pertanian &amp; Produk Pangan</option>
                        <option>Peralatan Kantor</option>
                      </select>
                      </div>
                      <div class="col-sm-2"><a href="post-moderation-editing.php" class="btn btn-sm btn-default">Edit</a></div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Kategori</td>
                  <td>
                    <div class="row">
                      <div class="col-sm-10">
                        <select class="form-control" disabled="">
                        <option>Teknologi &amp; Komunikasi</option>
                        <option>Kesehatan &amp; Kecantikan</option>
                        <option>Media, Musik 7 Buku</option>
                        <option>Pertanian &amp; Produk Pangan</option>
                        <option>Peralatan Kantor</option>
                      </select>
                      </div>
                      <div class="col-sm-2"><a href="post-moderation-editing.php" class="btn btn-sm btn-default">Edit</a></div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Kategori</td>
                  <td>
                    <div class="row">
                      <div class="col-sm-10">
                        <select class="form-control" disabled="">
                        <option>Teknologi &amp; Komunikasi</option>
                        <option>Kesehatan &amp; Kecantikan</option>
                        <option>Media, Musik 7 Buku</option>
                        <option>Pertanian &amp; Produk Pangan</option>
                        <option>Peralatan Kantor</option>
                      </select>
                      </div>
                      <div class="col-sm-2"><a href="post-moderation-editing.php" class="btn btn-sm btn-default">Edit</a></div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Produk Desc (id)</td>
                  <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</td>
                </tr>
                <tr>
                  <td>Produk Desc (eng)</td>
                  <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</td>
                </tr>
                <tr>
                  <td>Status</td>
                  <td>Aktif</td>
                </tr>
                <tr>
                  <td> </td>
                  <td><button type="button" class="btn btn-danger btn-sm" type="button" data-toggle="modal" data-target="#myModal">Nonaktifkan Produk ini</button></td>
                </tr>
              </tbody></table>

              </div>
            
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>



     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
