
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("modal-membership.html"); ?>

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Form Profil Users/Members
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">


          <div class="row">
            <div class="col-md-8">

              <div class="box">


                <form class="form-horizontal">
                  <div class="box-body">

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Membership Type</label>
                  <div class="col-sm-10">
                    <select class="form-control">
                    <option>Pilih Membership Type</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Durasi</label>
                  <div class="col-sm-10">
                    <select class="form-control">
                    <option>Pilih Durasi</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Alasan</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" rows="4" placeholder="Alasan ..."></textarea>
                  </div>
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer"><div class="pull-right">
                <button type="button" class="btn btn-primary" type="button" data-toggle="modal" data-target="#myModal">Simpan</button>
                <button type="submit" class="btn btn-danger">Batal</button>
              </div></div>
              <!-- /.box-footer -->
            </form>


              </div>
            
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>



     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
