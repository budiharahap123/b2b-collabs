
<?php include("header.html"); ?>
    <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker-bs3.css">

    <link rel="stylesheet" type="text/css" href="../plugins/timepicker/bootstrap-timepicker.min.css" />

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("modal-moderation.html"); ?>

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Member
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Date and time range -->
          <div class="form-group">
            <div class="row">
              <div class="col-sm-8">
                <label>Create Date:</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-clock-o"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="reservationtime">
                </div><!-- /.input group -->
              </div>

              <div class="col-sm-8">
                <label>Last Login:</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-clock-o"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="reservationtime">
                </div><!-- /.input group -->
              </div>

              <div class="col-sm-8">
                <label>Expire Date:</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-clock-o"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="reservationtime">
                </div><!-- /.input group -->
              </div>

            </div>
          </div>

              <div>
                <button type="submit" class="btn btn-primary">Search</button>
              </div>

              <div class="clear"></div><br />

         
<div class="row">
            <div class="col-md-12">


          <!-- SELECT2 EXAMPLE -->
          <div class="box">

            <div class="box-header with-border">
              <h3 class="box-title">Form Pencarian</h3>
            </div><!-- /.box-header -->
            <div class="box-body">

              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">


                <div>
                  <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead>
                <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="UserID: activate to sort column descending">ID Member</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="User Email: activate to sort column ascending">Name</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Email</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Create Date</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Update Date</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Last Login</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">ID Company</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">List Amount</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">City</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Status Member</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Activation Date</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Expire Date</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Pending Transfer</th>
                </tr>
                
                </thead>
                <tbody>
                
                <tr role="row">
                  <td>1</td>
                  <td>Emas Susanti</td>
                  <td>emas@susanti.com</td>
                  <td>07/03/2016</td>
                  <td>07/03/2016</td>
                  <td>07/03/2016</td>
                  <td>123123</td>
                  <td>13</td>
                  <td>Jakarta</td>
                  <td>Prioritas</td>
                  <td>07/03/2016</td>
                  <td>07/03/2016</td>
                  <td>Yes</td>
                </tr>
                <tr role="row">
                  <td>1</td>
                  <td>Emas Susanti</td>
                  <td>emas@susanti.com</td>
                  <td>07/03/2016</td>
                  <td>07/03/2016</td>
                  <td>07/03/2016</td>
                  <td>123123</td>
                  <td>13</td>
                  <td>Jakarta</td>
                  <td>Prioritas</td>
                  <td>07/03/2016</td>
                  <td>07/03/2016</td>
                  <td>Yes</td>
                </tr>
                <tr role="row">
                  <td>1</td>
                  <td>Emas Susanti</td>
                  <td>emas@susanti.com</td>
                  <td>07/03/2016</td>
                  <td>07/03/2016</td>
                  <td>07/03/2016</td>
                  <td>123123</td>
                  <td>13</td>
                  <td>Jakarta</td>
                  <td>Prioritas</td>
                  <td>07/03/2016</td>
                  <td>07/03/2016</td>
                  <td>Yes</td>
                </tr>
                <tr role="row">
                  <td>1</td>
                  <td>Emas Susanti</td>
                  <td>emas@susanti.com</td>
                  <td>07/03/2016</td>
                  <td>07/03/2016</td>
                  <td>07/03/2016</td>
                  <td>123123</td>
                  <td>13</td>
                  <td>Jakarta</td>
                  <td>Prioritas</td>
                  <td>07/03/2016</td>
                  <td>07/03/2016</td>
                  <td>Yes</td>
                </tr>
              </tbody>
                <tfoot>

                <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="UserID: activate to sort column descending">ID Member</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="User Email: activate to sort column ascending">Name</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Email</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Create Date</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Update Date</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Last Login</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">ID Company</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">List Amount</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">City</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Status Member</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Activation Date</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Expire Date</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Pending Transfer</th>
                </tr>

                </tfoot>
              </table></div></div><div class="col-sm-5"><div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div><div class="col-sm-7"><div class="dataTables_paginate paging_simple_numbers" id="example1_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="example1_previous"><a href="#" aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a></li><li class="paginate_button active"><a href="#" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" tabindex="0">3</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="4" tabindex="0">4</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="5" tabindex="0">5</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="6" tabindex="0">6</a></li><li class="paginate_button next" id="example1_next"><a href="#" aria-controls="example1" data-dt-idx="7" tabindex="0">Next</a></li></ul></div></div></div>
            </div>
            <!-- /.box-body -->
          </div>
            
            </div><!-- /.col (right) -->
          </div>

              <div class="form-group">
                <button type="submit" class="btn btn-primary">Download CSV</button>
              </div>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>


<!-- Select2 -->
    <script src="../plugins/select2/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="../plugins/input-mask/jquery.inputmask.js"></script>
    <script src="../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="../plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="../plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap color picker -->
    <script src="../plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="../plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- SlimScroll 1.3.0 -->


<!-- Page script -->
    <script>
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();

        //Datemask ../yyyy
        $("#datemask").inputmask("yyyy", {"placeholder": "yyyy"});
        //Datemask2 ../yyyy
        $("#datemask2").inputmask("yyyy", {"placeholder": "yyyy"});
        //Money Euro
        $("[data-mask]").inputmask();

        //Date range picker
        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'YYYY h:mm A'});
        //Date range as a button
        $('#daterange-btn').daterangepicker(
            {
              ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              },
              startDate: moment().subtract(29, 'days'),
              endDate: moment()
            },
        function (start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        );

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass: 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
          checkboxClass: 'icheckbox_minimal-red',
          radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });

        //Colorpicker
        $(".my-colorpicker1").colorpicker();
        //color picker with addon
        $(".my-colorpicker2").colorpicker();

        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });
      });
    </script>   
     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
