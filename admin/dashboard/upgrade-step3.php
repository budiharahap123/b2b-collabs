<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("modal-moderation.html"); ?>

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Metode Pembayaran
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-body">
                    <ul class="upgrade-member">
                      <li class="col-xs-12">
                        <div class="col-xs-12">
                          <ul class="progress-indicator">
                            <li class="completed">
                                <span class="stepbubble"></span>
                                1. Pilih Metode Pembayaran
                            </li>
                            <li class="completed">
                                <span class="stepbubble"></span>
                                2. Aktivasi Keanggotaan Terpilih
                            </li>
                            <li class="completed">
                                <span class="stepbubble"></span>
                                3. Konfirmasi Pembayaran
                            </li>
                          </ul>
                        </div>
                      </li>
                      <li class="col-xs-12">
                        <p>Setelah anda melakukan pembayaran, konfirmasikan melalui formulir di bawah ini:</p>
                      </li>
                      <li class="col-xs-12">
                        <table class="table table-striped table-bordered">
                          <tr>
                            <td class="col-md-2">Tipe Layanan</td>
                            <td class="col-md-10">Prioritas / Layanan SMS</td>
                          </tr>
                          <tr>
                            <td class="col-md-2">Status Langganan</td>
                            <td class="col-md-10">Perpanjangan</td>
                          </tr>
                          <tr>
                            <td class="col-md-2">Tanggal Transfer *</td>
                            <td class="col-md-12">
                              <ul class="row transfer-date">
                                <li class="col-md-4">
                                  <select id="day_start" name="day_start" class="form-control"> 
                                    <option>1</option>       
                                    <option>2</option>       
                                    <option>3</option>       
                                    <option>4</option>       
                                    <option>5</option>       
                                    <option>6</option>       
                                    <option>7</option>       
                                    <option>8</option>       
                                    <option>9</option>       
                                    <option>10</option>       
                                    <option>11</option>       
                                    <option>12</option>       
                                    <option>13</option>       
                                    <option>14</option>       
                                    <option>15</option>       
                                    <option>16</option>       
                                    <option>17</option>       
                                    <option>18</option>       
                                    <option>19</option>       
                                    <option>20</option>       
                                    <option>21</option>       
                                    <option>22</option>       
                                    <option>23</option>       
                                    <option>24</option>       
                                    <option>25</option>       
                                    <option>26</option>       
                                    <option>27</option>       
                                    <option>28</option>       
                                    <option>29</option>       
                                    <option>30</option>       
                                    <option>31</option>       
                                  </select>
                                </li>
                                <li class="col-md-4">
                                  <select id="day_start" name="month_start" class="form-control"> 
                                    <option>January</option>       
                                    <option>February</option>       
                                    <option>March</option>       
                                    <option>April</option>       
                                    <option>May</option>       
                                    <option>June</option>       
                                    <option>July</option>       
                                    <option>August</option>       
                                    <option>September</option>       
                                    <option>October</option>       
                                    <option>November</option>       
                                    <option>December</option> 
                                  </select>
                                </li>
                                <li class="col-md-4">
                                  <select id="year_start" name="year_start" class="form-control"> 
                                    <option>2009</option>       
                                    <option>2010</option>       
                                    <option>2011</option>       
                                    <option>2012</option>       
                                    <option>2013</option>       
                                    <option>2014</option>       
                                    <option>2015</option>       
                                    <option>2016</option>       
                                    <option>2017</option>       
                                    <option>2018</option>       
                                  </select>
                                </li>
                                <li class="col-md-12"><div class="notif-bar">Tanggal Transfer Harus Diisi!</div></li>
                              </ul>
                            </td>
                          </tr>
                          <tr>
                            <td class="col-md-2">Jumlah Pembayaran *</td>
                            <td class="col-md-10"><input class="form-control" placeholder="Rp." type="text"></td>
                          </tr>
                          <tr>
                            <td class="col-md-2">Bank Penerima</td>
                            <td class="col-md-10">BCA / Mandiri</td>
                          </tr>
                          <tr>
                            <td class="col-md-2">Nomor Telepon *</td>
                            <td class="col-md-10"><input class="form-control" placeholder="02XXXXXX" type="text"></td>
                          </tr>
                          <tr>
                            <td class="col-md-2">Nama Pengirim *</td>
                            <td class="col-md-10"><input class="form-control" placeholder="Nama Pengirim" type="text"></td>
                          </tr>
                          <tr>
                            <td class="col-md-2">Bank Pengirim *</td>
                            <td class="col-md-10"><input class="form-control" placeholder="Nama Bank Pengirim" type="text"></td>
                          </tr>
                          <tr>
                            <td class="col-md-2">Bukti transaksi *</td>
                            <td class="col-md-10">
                              <ul class="row transfer-date">
                                <li class="col-md-12"><input type="file"></li> 
                                <li class="col-md-12"><div class="notif-bar">Bukti Transaksi Harus Diisi!</div></li>
                              </ul>
                            </td>
                          </tr>
                          <tr>
                            <td class="col-md-2">Keterangan Tambahan</td>
                            <td class="col-md-10">
                              <textarea class="form-control"></textarea>
                            </td>
                          </tr>
                        </table>
                      </li>
                      <li class="col-xs-12">
                        <div class="text-center">
                          <a href="upgrade-step-success.php" class="btn btn-warning">Kirim</a>
                        </div>
                      </li>
                    </ul>                    
                </div>
              </div>
              </div>
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>



     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
