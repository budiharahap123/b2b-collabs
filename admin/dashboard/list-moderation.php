
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Moderasi Produk
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-12">

          <!-- SELECT2 EXAMPLE -->
          <div class="box">

            <div class="box-header with-border">
              <h3 class="box-title">Pilih Moderasi</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                      <tbody>
                      <tr>
                        <td>Status</td>
                        <td>
                          <div>
                            <label>
                              <input type="radio" name="status"> Pending
                            </label>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td>Kategori
                        </td>
                        <td>
                          <table class="full">
                            <tbody>
                              <tr>
                                <td>
                                  <table class="full"><tbody><tr>
                                    <tr><td><input type="radio" name="kategori"> Agraris</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Pakaian & Mode</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Otomotif</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Agraris</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Pakaian & Mode</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Otomotif</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Agraris</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Pakaian & Mode</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Otomotif</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Agraris</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Pakaian & Mode</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Otomotif</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Pakaian & Mode</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Otomotif</td></tr>
                                  </tbody></table>
                                </td>
                                <td>
                                  <table class="full"><tbody><tr>
                                    <tr><td><input type="radio" name="kategori"> Agraris</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Pakaian & Mode</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Otomotif</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Agraris</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Pakaian & Mode</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Otomotif</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Agraris</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Pakaian & Mode</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Otomotif</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Agraris</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Pakaian & Mode</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Otomotif</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Pakaian & Mode</td></tr>
                                    <tr><td><input type="radio" name="kategori"> Otomotif</td></tr>
                                  </tbody></table>
                                </td>
                              </tr>
                            </tbody>
                          </table>

                        </td>
                      </tr>
                      <tr>
                        <td>Total Lock</td>
                        <td>
                          <table class="full">
                            <tbody>
                              <tr>
                                <td>
                                  <table class="full"><tbody><tr>
                                    <td><input type="radio" name="lock"> 50</td>
                                    <td><input type="radio" name="lock"> 100</td>
                                    <td><input type="radio" name="lock"> 150</td>
                                    <td><input type="radio" name="lock"> 200</td>
                                  </tr></tbody></table>
                                </td>
                              </tr>
                            </tbody>
                          </table>

                        </td>
                      </tr>

                      </tbody>
                    </table>
                
                </div><!-- /.col -->

                <div class="text-center">
                  <button class="btn btn-primary">Lock Now</button>
                </div>

              </div><!-- /.row -->
            </div><!-- /.box-body -->

          </div>
            
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<!-- <?php // include("footer.html"); ?>-->

<?php include("rightside.html"); ?> 

<?php include("general-script.html"); ?>

     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
