
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Upload Image <small>Index</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
  

          <div class="row">

            <div class="col-md-12">
              <div class="box">
                <br>
                <br>
                <div class="box-body">
                <div class="input-group col-md-4">
                  <form role="form">
                    <input type="file" class="filestyle" data-icon="false">
                  </form>
              </div>
              </div>
              </div>
              <div class="box">
                <div class="box-body">
                  <div class="col-md-2">
                   <h4>List Url</h4>
                  </div>
                  <div class="col-md-7">
                    <div class="input-group">
                      <input type="text" class="form-control">
                      <span class="input-group-btn">
                        <button class="btn btn-primary btn-flat" type="button">Search</button>
                      </span>
                    </div>
                  </div>
                  <br>
                  <div class="row"><div class="col-sm-12"><div class="table-responsive"><table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead>
                <tr role="row"><th class="sorting_asc col-md-1" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="UserID: activate to sort column descending">ID</th><th class="sorting_asc col-md-9" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="UserID: activate to sort column descending">URL</th><th class="col-md-2" tabindex="0" aria-controls="example1" rowspan="1" colspan="2">Action</th>
                </tr>
                </thead>
                <tr role="row" class="odd">
                  <td class="sorting_1">1</td>
                  <td><a href="www.indonetwork.co.id/file/banner1" target="_blank">www.indonetwork.co.id/file/banner1</a></td>
                  <td><a href="#" class="btn btn-flat btn-default">Detail</a></td><td><a href="#" class="btn btn-flat btn-danger">Delete</a></button></td>
                </tr>
                <tr role="row" class="even">
                  <td class="sorting_1">2</td>
                  <td><a href="www.indonetwork.co.id/file/banner1" target="_blank">www.indonetwork.co.id/file/banner1</a></td>
                  <td><a href="#" class="btn btn-flat btn-default">Detail</a></td><td><a href="#" class="btn btn-flat btn-danger">Delete</a></button></td>
                </tr>
                <tr role="row" class="odd">
                  <td class="sorting_1">3</td>
                  <td><a href="www.indonetwork.co.id/file/banner2" target="_blank">www.indonetwork.co.id/file/banner2</a></td>
                  <td><a href="#" class="btn btn-flat btn-default">Detail</a></td><td><a href="#" class="btn btn-flat btn-danger">Delete</a></button></td>
                </tr>
                <tr role="row" class="even">
                  <td class="sorting_1">4</td>
                  <td><a href="www.indonetwork.co.id/file/banner1" target="_blank">www.indonetwork.co.id/file/banner1</a></td>
                  <td><a href="#" class="btn btn-flat btn-default">Detail</a></td><td><a href="#" class="btn btn-flat btn-danger">Delete</a></button></td>
                </tr>
                <tr role="row" class="odd">
                  <td class="sorting_1">5</td>
                  <td><a href="www.indonetwork.co.id/file/banner1" target="_blank">www.indonetwork.co.id/file/banner1</a></td>
                  <td><a href="#" class="btn btn-flat btn-default">Detail</a></td><td><a href="#" class="btn btn-flat btn-danger">Delete</a></button></td>
                </tr>
                <tr role="row" class="even">
                  <td class="sorting_1">6</td>
                  <td><a href="www.indonetwork.co.id/file/banner1" target="_blank">www.indonetwork.co.id/file/banner1</a></td>
                  <td><a href="#" class="btn btn-flat btn-default">Detail</a></td><td><a href="#" class="btn btn-flat btn-danger">Delete</a></button></td>
                </tr>
                <tr role="row" class="odd">
                  <td class="sorting_1">8</td>
                  <td><a href="www.indonetwork.co.id/file/banner1" target="_blank">www.indonetwork.co.id/file/banner1</a></td>
                  <td><a href="#" class="btn btn-flat btn-default">Detail</a></td><td><a href="#" class="btn btn-flat btn-danger">Delete</a></button></td>
                </tr>
                <tr role="row" class="even">
                  <td class="sorting_1">9</td>
                  <td><a href="www.indonetwork.co.id/file/banner1" target="_blank">www.indonetwork.co.id/file/banner1</a></td>
                  <td><a href="#" class="btn btn-flat btn-default">Detail</a></td><td><a href="#" class="btn btn-flat btn-danger">Delete</a></button></td>
                </tr>
                <tr role="row" class="odd">
                  <td class="sorting_1">10</td>
                  <td><a href="www.indonetwork.co.id/file/banner1" target="_blank">www.indonetwork.co.id/file/banner1</a></td>
                  <td><a href="#" class="btn btn-flat btn-default">Detail</a></td><td><a href="#" class="btn btn-flat btn-danger">Delete</a></button></td>
                </tr>
                </table>
                </div>
              </div>
            </div><!-- /.col (right) -->

          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>



     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
      <script type="text/javascript" src="../dist/js/bootstrap-filestyle.min.js"> </script>
      <script type="text/javascript">
        $(":file").filestyle({icon: false, buttonText: "Upload Image"});
      </script>
  </body>
</html>
