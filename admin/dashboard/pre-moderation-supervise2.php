
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">
      
<?php include("modal-supervise.html"); ?>

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Moderasi Produk by User
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-12">

          <!-- SELECT2 EXAMPLE -->
          <div class="box">


              <div class="box-header with-border">
                <h3 class="box-title">List Moderasi</h3>
              </div><!-- /.box-header -->
              <div class="box-body">
              <form class="form-horizontal">

                <div class="box-body">
                    <div class="form-group">
                    <label class="col-sm-2">Email/Username</label>
                    <div class="col-sm-9"><input type="text" class="form-control" placeholder="Email/Username"></div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-2">Product ID</label>
                    <div class="col-sm-9"><input type="text" class="form-control" placeholder="produk ID"></div>
                    </div>

                    <div class="with-border form-group">
                      <label class="col-sm-2">Lock</label>
                      <div class="col-sm-9">
                        <ul class="list-unstyled">
                          <li><input type="radio" name="lock"> 50</li>
                          <li><input type="radio" name="lock"> 100</li>
                          <li><input type="radio" name="lock"> 150</li>
                          <li><input type="radio" name="lock"> 200</li>
                        </ul>
                      </div>
                    </div>
                </div>

                </form>
                  <div class="text-left">
                    <a href="pre-moderation-supervise-edit.php" class="btn btn-big btn-primary">Cek Produk Sekarang</a>
                  </div>

              </div><!-- /.box-body -->


          </div>
            
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>

     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
