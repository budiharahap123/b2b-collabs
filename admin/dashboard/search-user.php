
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Cari Users/Members
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-12">

          <!-- SELECT2 EXAMPLE -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Form Pencarian</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">

                  <div class="input-group">
                  <input type="text" class="form-control">
                <div class="input-group-btn">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Username
                    &nbsp;&nbsp;<span class="fa fa-caret-down"></span></button>
                  <ul class="dropdown-menu">
                    <li><a href="#">Username</a></li>
                    <li><a href="#">Email</a></li>
                    <li><a href="#">No Handphone</a></li>
                    <li><a href="#">Nama Perusahaan</a></li>
                    <li><a href="#">Nama Company Page</a></li>                   
                  </ul>
                  <button type="button" class="btn btn-primary">Cari</button>
                </div>
                <!-- /btn-group -->
                
              </div>

                </div><!-- /.col -->
                
              </div><!-- /.row -->
            </div><!-- /.box-body -->

            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-12"><div class="dataTables_length" id="example1_length"><label>Tampilkan&nbsp;&nbsp;<select name="example1_length" aria-controls="example1" class="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select>&nbsp;&nbsp;data</label></div></div></div></div><div class="row"><div class="col-sm-12"><div class="table-responsive"><table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead>
                <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="UserID: activate to sort column descending">UserID</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Username: activate to sort column ascending">Username</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="User Email: activate to sort column ascending">Email</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Membership Tipe</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Nama</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">No HP</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Nama Perusahaan</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Company Page</th><th tabindex="0" aria-controls="example1" rowspan="1" >Tombol</th>
                </tr>
                </thead>
                <tbody>
                
                
                <tr role="row" class="odd">
                  <td class="sorting_1">123456</td>
                  <td>Firefox 1.0</td>
                  <td><a href="">budi.pardomuan@yahoo.com</a></td>
                  <td>Layanan SMS</td><td>Budiadiliansyah</td><td>0856 123 456</td><td>PT Handal Jaya Berjaya</td><td><a href="http://handaljaya.indonetwork.co.id">handaljaya.indonetwork.co.id</a></td>
                  <td><button type="button" class="btn btn-default btn-xs" title="Lihat Profile Detail" onclick="location.href='user-page-profile.php'"><i class="fa fa-fw fa-user"></i></button>
                      <button type="button" class="btn btn-default btn-xs" title="Lihat Membership Detail" onclick="location.href='membership-page-profile.php'"><i class="fa fa-fw fa-users"></i></button></td>
                </tr><tr role="row" class="even">
                  <td class="sorting_1">123456</td>
                  <td>Firefox 1.5</td>
                  <td><a href="">budi.pardomuan@gmail.com</a></td>
                  <td>Prioritas</td><td>Budiadiliansyah</td><td>0856 123 456</td><td>PT Handal Jaya Berjaya</td><td><a href="http://handaljaya.indonetwork.co.id">handaljaya.indonetwork.co.id</a></td>
                  <td><button type="button" class="btn btn-default btn-xs" title="Lihat Profile Detail" onclick="location.href='user-page-profile.php'"><i class="fa fa-fw fa-user"></i></button>
                      <button type="button" class="btn btn-default btn-xs" title="Lihat Membership Detail" onclick="location.href='membership-page-profile.php'"><i class="fa fa-fw fa-users"></i></button></td>
                </tr><tr role="row" class="odd">
                  <td class="sorting_1">123456</td>
                  <td>Firefox 2.0</td>
                  <td><a href="">budi.pardomuan@yahoo.com</a></td>
                  <td>Layanan SMS</td><td>Budiadiliansyah</td><td>0856 123 456</td><td>PT Handal Jaya Berjaya</td><td><a href="http://handaljaya.indonetwork.co.id">handaljaya.indonetwork.co.id</a></td>
                  <td><button type="button" class="btn btn-default btn-xs" title="Lihat Profile Detail" onclick="location.href='user-page-profile.php'"><i class="fa fa-fw fa-user"></i></button>
                      <button type="button" class="btn btn-default btn-xs" title="Lihat Membership Detail" onclick="location.href='membership-page-profile.php'"><i class="fa fa-fw fa-users"></i></button></td>
                </tr><tr role="row" class="even">
                  <td class="sorting_1">123456</td>
                  <td>Firefox 3.0</td>
                  <td><a href="">budi.pardomuan@mataharimall.com</a></td>
                  <td>Layanan SMS</td><td>Budiadiliansyah</td><td>0856 123 456</td><td>PT Handal Jaya Berjaya</td><td><a href="http://handaljaya.indonetwork.co.id">handaljaya.indonetwork.co.id</a></td>
                  <td><button type="button" class="btn btn-default btn-xs" title="Lihat Profile Detail" onclick="location.href='user-page-profile.php'"><i class="fa fa-fw fa-user"></i></button>
                      <button type="button" class="btn btn-default btn-xs" title="Lihat Membership Detail" onclick="location.href='membership-page-profile.php'"><i class="fa fa-fw fa-users"></i></button></td>
                </tr><tr role="row" class="odd">
                  <td class="sorting_1">123456</td>
                  <td>Camino 1.0</td>
                  <td><a href="">budi.pardomuan@gmail.com</a></td>
                  <td>Prioritas</td><td>Budiadiliansyah</td><td>0856 123 456</td><td>PT Handal Jaya Berjaya</td><td><a href="http://handaljaya.indonetwork.co.id">handaljaya.indonetwork.co.id</a></td>
                  <td><button type="button" class="btn btn-default btn-xs" title="Lihat Profile Detail" onclick="location.href='user-page-profile.php'"><i class="fa fa-fw fa-user"></i></button>
                      <button type="button" class="btn btn-default btn-xs" title="Lihat Membership Detail" onclick="location.href='membership-page-profile.php'"><i class="fa fa-fw fa-users"></i></button></td>
                </tr><tr role="row" class="even">
                  <td class="sorting_1">123456</td>
                  <td>Camino 1.5</td>
                  <td><a href="">budi.pardomuan@mataharibiz.com</a></td>
                  <td>Gratis</td><td>Budiadiliansyah</td><td>0856 123 456</td><td>PT Handal Jaya Berjaya</td><td><a href="http://handaljaya.indonetwork.co.id">handaljaya.indonetwork.co.id</a></td>
                  <td><button type="button" class="btn btn-default btn-xs" title="Lihat Profile Detail" onclick="location.href='user-page-profile.php'"><i class="fa fa-fw fa-user"></i></button>
                      <button type="button" class="btn btn-default btn-xs" title="Lihat Membership Detail" onclick="location.href='membership-page-profile.php'"><i class="fa fa-fw fa-users"></i></button></td>
                </tr><tr role="row" class="odd">
                  <td class="sorting_1">123456</td>
                  <td>Netscape 7.2</td>
                  <td><a href="">budi.pardomuan@gmail.com</a></td>
                  <td>Prioritas</td><td>Budiadiliansyah</td><td>0856 123 456</td><td>PT Handal Jaya Berjaya</td><td><a href="http://handaljaya.indonetwork.co.id">handaljaya.indonetwork.co.id</a></td>
                  <td><button type="button" class="btn btn-default btn-xs" title="Lihat Profile Detail" onclick="location.href='user-page-profile.php'"><i class="fa fa-fw fa-user"></i></button>
                      <button type="button" class="btn btn-default btn-xs" title="Lihat Membership Detail" onclick="location.href='membership-page-profile.php'"><i class="fa fa-fw fa-users"></i></button></td>
                </tr><tr role="row" class="even">
                  <td class="sorting_1">123456</td>
                  <td>Netscape Username 8</td>
                  <td><a href="">budi.pardomuan@mataharimall.com</a></td>
                  <td>Prioritas</td><td>Budiadiliansyah</td><td>0856 123 456</td><td>PT Handal Jaya Berjaya</td><td><a href="http://handaljaya.indonetwork.co.id">handaljaya.indonetwork.co.id</a></td>
                  <td><button type="button" class="btn btn-default btn-xs" title="Lihat Profile Detail" onclick="location.href='user-page-profile.php'"><i class="fa fa-fw fa-user"></i></button>
                      <button type="button" class="btn btn-default btn-xs" title="Lihat Membership Detail" onclick="location.href='membership-page-profile.php'"><i class="fa fa-fw fa-users"></i></button></td>
                </tr><tr role="row" class="odd">
                  <td class="sorting_1">123456</td>
                  <td>Netscape Navigator 9</td>
                  <td><a href="">budi.pardomuan@mataharibiz.com</a></td>
                  <td>Prioritas</td><td>Budiadiliansyah</td><td>0856 123 456</td><td>PT Handal Jaya Berjaya</td><td><a href="http://handaljaya.indonetwork.co.id">handaljaya.indonetwork.co.id</a></td>
                  <td><button type="button" class="btn btn-default btn-xs" title="Lihat Profile Detail" onclick="location.href='user-page-profile.php'"><i class="fa fa-fw fa-user"></i></button>
                      <button type="button" class="btn btn-default btn-xs" title="Lihat Membership Detail" onclick="location.href='membership-page-profile.php'"><i class="fa fa-fw fa-users"></i></button></td>
                </tr><tr role="row" class="even">
                  <td class="sorting_1">123456</td>
                  <td>Mozilla 1.0</td>
                  <td><a href="">budi.pardomuan@yahoo.com</a></td>
                  <td>Gratis</td><td>Budiadiliansyah</td><td>0856 123 456</td><td>PT Handal Jaya Berjaya</td><td><a href="http://handaljaya.indonetwork.co.id">handaljaya.indonetwork.co.id</a></td>
                  <td><button type="button" class="btn btn-default btn-xs" title="Lihat Profile Detail" onclick="location.href='user-page-profile.php'"><i class="fa fa-fw fa-user"></i></button>
                      <button type="button" class="btn btn-default btn-xs" title="Lihat Membership Detail" onclick="location.href='membership-page-profile.php'"><i class="fa fa-fw fa-users"></i></button></td>
                </tr></tbody>
                <tfoot>
                <tr><th rowspan="1" colspan="1">UserID</th><th rowspan="1" colspan="1">Username</th><th rowspan="1" colspan="1">Email</th><th rowspan="1" colspan="1">Membership Type</th><th rowspan="1" colspan="1">Nama</th><th rowspan="1" colspan="1">No HP</th><th rowspan="1" colspan="1">Nama Perusahaan</th><th rowspan="1" colspan="1">Company Page</th><th rowspan="1" colspan="1"> </th></tr>
                </tfoot>
              </table></div></div></div><div class="row"><div class="col-sm-5"><div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div><div class="col-sm-7"><div class="dataTables_paginate paging_simple_numbers" id="example1_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="example1_previous"><a href="#" aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a></li><li class="paginate_button active"><a href="#" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" tabindex="0">3</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="4" tabindex="0">4</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="5" tabindex="0">5</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="6" tabindex="0">6</a></li><li class="paginate_button next" id="example1_next"><a href="#" aria-controls="example1" data-dt-idx="7" tabindex="0">Next</a></li></ul></div></div></div></div>
            </div>
            <!-- /.box-body -->
          </div>
            
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>



     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
