
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Moderasi Produk
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-12">

          <!-- SELECT2 EXAMPLE -->
          <div class="box">

            <div class="box-header with-border">
              <h3 class="box-title">List Moderasi Pending</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">

                    <table class="table table-bordered table-striped dataTable">
                      <thead>
                        <tr>
                          <th>Judul Kategori</th>
                          <th>Total Pending</th>
                        </tr>
                      </thead>
                      <tbody>
                      <tr>
                        <td>Agraris</td>
                        <td>786</td>
                      </tr>
                      <tr>
                        <td>Pakaian & Mode</td>
                        <td>112</td>
                      </tr>
                      <tr>
                        <td>Otomotif</td>
                        <td>234</td>
                      </tr>
                      <tr>
                        <td>Layanan Bisnis</td>
                        <td>765</td>
                      </tr>
                      <tr>
                        <td>Kimia</td>
                        <td>34</td>
                      </tr>
                      <tr>
                        <td>Komputer & Software</td>
                        <td>64</td>
                      </tr>
                      <tr>
                        <td>Konstruksi & Real Estate</td>
                        <td>111</td>
                      </tr>
                      <tr>
                        <td>Elektronik Konsumen</td>
                        <td>421</td>
                      </tr>
                      <tr>
                        <td>Elektronik & Elektrik</td>
                        <td>345</td>
                      </tr>
                      <tr>
                        <td>Enerji</td>
                        <td>45</td>
                      </tr>
                      <tr>
                        <td>Lingkungan</td>
                        <td>887</td>
                      </tr>
                      <tr>
                        <td>Persediaan Lebih</td>
                        <td>65</td>
                      </tr>
                      <tr>
                        <td>Makanan & Minuman</td>
                        <td>42</td>
                      </tr>
                      <tr>
                        <td>Mebel</td>
                        <td>113</td>
                      </tr>
                    </tbody>
                  </table>

                </div><!-- /.col -->
                <div class="col-md-6">

                    <table class="table table-bordered table-striped dataTable">
                      <thead>
                        <tr>
                          <th>Judul Kategori</th>
                          <th>Total Pending</th>
                        </tr>
                      </thead>
                      <tbody>
                      <tr>
                        <td>Kerajinan & Souvenir</td>
                        <td>85</td>
                      </tr>
                      <tr>
                        <td>Kesehatan & Kecantikan</td>
                        <td>88</td>
                      </tr>
                      <tr>
                        <td>Kebutuhan Rumah Tangga</td>
                        <td>99</td>
                      </tr>
                      <tr>
                        <td>Kebutuhan Industri</td>
                        <td>121</td>
                      </tr>
                      <tr>
                        <td>Mineral & Logam</td>
                        <td>432</td>
                      </tr>
                      <tr>
                        <td>Kebutuhan Kantor</td>
                        <td>76</td>
                      </tr>
                      <tr>
                        <td>Pembungkus & Kertas</td>
                        <td>85</td>
                      </tr>
                      <tr>
                        <td>Percetakan & Penerbitan</td>
                        <td>112</td>
                      </tr>
                      <tr>
                        <td>Keamanan & Perlindungan</td>
                        <td>84</td>
                      </tr>
                      <tr>
                        <td>Olahraga & Hiburan</td>
                        <td>58</td>
                      </tr>
                      <tr>
                        <td>Tekstil & Kulit</td>
                        <td>45</td>
                      </tr>
                      <tr>
                        <td>Telekomunikasi</td>
                        <td>62</td>
                      </tr>
                      <tr>
                        <td>Mainan</td>
                        <td>79</td>
                      </tr>
                      <tr>
                        <td>Transportasi</td>
                        <td>69</td>
                      </tr>
                    </tbody>
                  </table>

                </div><!-- /.col -->


              </div><!-- /.row -->
            </div><!-- /.box-body -->

          </div>
            
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>

     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
