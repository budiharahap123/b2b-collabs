
<?php include("header.html"); ?>
    <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker-bs3.css">

    <link rel="stylesheet" type="text/css" href="../plugins/timepicker/bootstrap-timepicker.min.css" />

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Moderasi Produk
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Date and time range -->
          <div class="form-group">
            <label>Date and time range:</label>
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-clock-o"></i>
              </div>
              <input type="text" class="form-control pull-right" id="reservationtime">
            </div><!-- /.input group -->
          </div><!-- /.form group -->

          <div class="row">
            <div class="col-md-12">

          <!-- SELECT2 EXAMPLE -->
          <div class="box">

            <div class="box-header with-border">
              <h3 class="box-title">List Performance Moderator</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">

                    <table class="table table-bordered table-striped dataTable">
                      <thead>
                        <tr>
                          <th>Email</th>
                          <th>Approve</th>
                          <th>Reject</th>
                          <th>Supervise</th>
                        </tr>
                      </thead>
                      <tbody>
                      <tr>
                        <td>nama.moderator@indonetwork.co.id</td>
                        <td><span class="label label-success">786</span></td>
                        <td><span class="label label-danger">2546</span></td>
                        <td><span class="label label-warning">222</span></td>
                      </tr>
                      <tr>
                        <td>nama.moderator@indonetwork.co.id</td>
                        <td><span class="label label-success">786</span></td>
                        <td><span class="label label-danger">2546</span></td>
                        <td><span class="label label-warning">222</span></td>
                      </tr>
                      <tr>
                        <td>nama.moderator@indonetwork.co.id</td>
                        <td><span class="label label-success">786</span></td>
                        <td><span class="label label-danger">2546</span></td>
                        <td><span class="label label-warning">222</span></td>
                      </tr>
                      <tr>
                        <td>nama.moderator@indonetwork.co.id</td>
                        <td><span class="label label-success">786</span></td>
                        <td><span class="label label-danger">2546</span></td>
                        <td><span class="label label-warning">222</span></td>
                      </tr>
                      <tr>
                        <td>nama.moderator@indonetwork.co.id</td>
                        <td><span class="label label-success">786</span></td>
                        <td><span class="label label-danger">2546</span></td>
                        <td><span class="label label-warning">222</span></td>
                      </tr>
                      <tr>
                        <td>nama.moderator@indonetwork.co.id</td>
                        <td><span class="label label-success">786</span></td>
                        <td><span class="label label-danger">2546</span></td>
                        <td><span class="label label-warning">222</span></td>
                      </tr>
                      <tr>
                        <td>nama.moderator@indonetwork.co.id</td>
                        <td><span class="label label-success">786</span></td>
                        <td><span class="label label-danger">2546</span></td>
                        <td><span class="label label-warning">222</span></td>
                      </tr>
                      <tr>
                        <td>nama.moderator@indonetwork.co.id</td>
                        <td><span class="label label-success">786</span></td>
                        <td><span class="label label-danger">2546</span></td>
                        <td><span class="label label-warning">222</span></td>
                      </tr>
                    </tbody>
                  </table>

                  <div><button class="btn btn-default">Export To .CSV</button></div>

                </div><!-- /.col -->


              </div><!-- /.row -->
            </div><!-- /.box-body -->

          </div>
            
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>

    <!-- Select2 -->
    <script src="../plugins/select2/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="../plugins/input-mask/jquery.inputmask.js"></script>
    <script src="../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="../plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="../plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap color picker -->
    <script src="../plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="../plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- SlimScroll 1.3.0 -->


<!-- Page script -->
    <script>
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();

        //Datemask ../yyyy
        $("#datemask").inputmask("yyyy", {"placeholder": "yyyy"});
        //Datemask2 ../yyyy
        $("#datemask2").inputmask("yyyy", {"placeholder": "yyyy"});
        //Money Euro
        $("[data-mask]").inputmask();

        //Date range picker
        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'YYYY h:mm A'});
        //Date range as a button
        $('#daterange-btn').daterangepicker(
            {
              ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              },
              startDate: moment().subtract(29, 'days'),
              endDate: moment()
            },
        function (start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        );

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass: 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
          checkboxClass: 'icheckbox_minimal-red',
          radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });

        //Colorpicker
        $(".my-colorpicker1").colorpicker();
        //color picker with addon
        $(".my-colorpicker2").colorpicker();

        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });
      });
    </script>     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
