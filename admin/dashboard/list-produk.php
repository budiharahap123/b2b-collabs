
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("modal-moderation.html"); ?>

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Moderasi Produk
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-12">

          <!-- SELECT2 EXAMPLE -->
          <div class="box">

            <div class="box-header with-border">
              <h3 class="box-title">Form Pencarian</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="row">

                <div class="col-md-2">
                  <select name="example1_length" aria-controls="example1" class="form-control input-sm"><option value="10">Active</option><option value="25">Pause</option></select>
                </div>

                <div class="col-md-7">

                  <div class="input-group">
                    <input type="text" class="form-control">
                    <span class="input-group-btn">
                      <button class="btn btn-primary btn-flat" type="button">Search</button>
                    </span>
                  </div>

                </div><!-- /.col -->

                <div class="col-md-3">

                  <div class="input-group pull-right">
                      <button class="btn btn-primary btn-flat" type="button">Add New Product</button>
                  </div>

                </div><!-- /.col -->
                
              </div><!-- /.row -->
            </div><!-- /.box-body -->

            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-12"><div class="dataTables_length" id="example1_length"><label>Tampilkan&nbsp;&nbsp;<select name="example1_length" aria-controls="example1" class="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select>&nbsp;&nbsp;data</label></div></div></div></div><div class="row"><div class="col-sm-12"><div class="table-responsive"><table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead>
                <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="UserID: activate to sort column descending"> </th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="User Email: activate to sort column ascending">Title Product</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Harga</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Update</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Membership Type: activate to sort column ascending">Status</th>
                </tr>
                </tr>
                </thead>
                <tbody>
                
                
                <tr role="row" class="odd">
                  <td><a href=""><img src="../../assets/images/product-01.jpg" width="100"></a></li></td><td class="sorting_1">Nike Lunar Glide 7</td>
                  </td><td>150.000</td><td>07/03/2016</td>
                  <td>Prioritas</td>
                </tr><tr role="row" class="even">
                  <td><a href=""><img src="../../assets/images/product-01.jpg" width="100"></a></li></td><td class="sorting_1">Nike Lunar Glide 7</td>
                  </td><td>150.000</td><td>07/03/2016</td>
                  <td>Prioritas</td>
                </tr><tr role="row" class="odd">
                  <td><a href=""><img src="../../assets/images/product-01.jpg" width="100"></a></li></td><td class="sorting_1">Nike Lunar Glide 7</td>
                  </td><td>150.000</td><td>07/03/2016</td>
                  <td>Prioritas</td>
                </tr><tr role="row" class="even">
                  <td><a href=""><img src="../../assets/images/product-01.jpg" width="100"></a></li></td><td class="sorting_1">Nike Lunar Glide 7</td>
                  </td><td>150.000</td><td>07/03/2016</td>
                  <td>Prioritas</td>
                </tr><tr role="row" class="odd">
                  <td><a href=""><img src="../../assets/images/product-01.jpg" width="100"></a></li></td><td class="sorting_1">Nike Lunar Glide 7</td>
                  </td><td>150.000</td><td>07/03/2016</td>
                  <td>Prioritas</td>
                </tr><tr role="row" class="even">
                  <td><a href=""><img src="../../assets/images/product-01.jpg" width="100"></a></li></td><td class="sorting_1">Nike Lunar Glide 7</td>
                  </td><td>150.000</td><td>07/03/2016</td>
                  <td>Prioritas</td>
                </tr><tr role="row" class="odd">
                  <td><a href=""><img src="../../assets/images/product-01.jpg" width="100"></a></li></td><td class="sorting_1">Nike Lunar Glide 7</td>
                  </td><td>150.000</td><td>07/03/2016</td>
                  <td>Prioritas</td>
                </tr><tr role="row" class="even">
                  <td><a href=""><img src="../../assets/images/product-01.jpg" width="100"></a></li></td><td class="sorting_1">Nike Lunar Glide 7</td>
                  </td><td>150.000</td><td>07/03/2016</td>
                  <td>Prioritas</td>
                </tr><tr role="row" class="odd">
                  <td><a href=""><img src="../../assets/images/product-01.jpg" width="100"></a></li></td><td class="sorting_1">Nike Lunar Glide 7</td>
                  </td><td>150.000</td><td>07/03/2016</td>
                  <td>Prioritas</td>
                </tr><tr role="row" class="even">
                  <td><a href=""><img src="../../assets/images/product-01.jpg" width="100"></a></li></td><td class="sorting_1">Nike Lunar Glide 7</td>
                  </td><td>150.000</td><td>07/03/2016</td>
                  <td>Prioritas</td>
                </tr></tbody>
                <tfoot>
                <tr><th rowspan="1" colspan="1"> </th><th rowspan="1" colspan="1">Title Product</th><th rowspan="1" colspan="1">Harga</th><th rowspan="1" colspan="1">Update</th><th rowspan="1" colspan="1">Status</th></tr>
                </tfoot>
              </table></div></div></div><div class="row"><div class="col-sm-5"><div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div><div class="col-sm-7"><div class="dataTables_paginate paging_simple_numbers" id="example1_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="example1_previous"><a href="#" aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a></li><li class="paginate_button active"><a href="#" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" tabindex="0">3</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="4" tabindex="0">4</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="5" tabindex="0">5</a></li><li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="6" tabindex="0">6</a></li><li class="paginate_button next" id="example1_next"><a href="#" aria-controls="example1" data-dt-idx="7" tabindex="0">Next</a></li></ul></div></div></div></div>
            </div>
            <!-- /.box-body -->
          </div>
            
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>



     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
