
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("modal-change-category.html"); ?>

<?php include("modal-change-category-confirmation.html"); ?>


<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Category Change
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-12">

          <!-- SELECT2 EXAMPLE -->
          <div class="box">

            <div class="box-header with-border">
              <h3 class="box-title">List Produk</h3>
            </div><!-- /.box-header -->

            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-12"><div class="dataTables_length" id="example1_length"><label>Tampilkan&nbsp;&nbsp;<select name="example1_length" aria-controls="example1" class="form-control input-sm"><option value="10">50</option><option value="10">100</option><option value="25">200</option></select>&nbsp;&nbsp;data</label></div></div></div></div><div class="row"><div class="col-sm-12"><div class="table-responsive"><table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead>
                <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1"  aria-sort="ascending" aria-label="UserID: activate to sort column descending">ID</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"  aria-label="User Email: activate to sort column ascending">Title Product</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"  aria-label="Membership Type: activate to sort column ascending">Current category</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"  aria-label="Membership Type: activate to sort column ascending">New Category</th><th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"  aria-label="Membership Type: activate to sort column ascending">Status</th><th tabindex="0" aria-controls="example1" rowspan="1" >Action</th>
                </tr>
                </tr>
                </thead>
                <tbody>
                
                
                <tr role="row" class="odd">
                  <td class="sorting_1">154</td>
                  <td><a href="">Baju Branding</a></td><td><small>Teknologi & Komunikasi</small></td><td><small>Handphone & Tablet</small></td><td>Aktif</td>
                  <td><button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">Ubah</button> <button type="button" class="btn btn-warning btn-xs">Reset</button></td>
                </tr><tr role="row" class="even">
                  <td class="sorting_1">154</td>

                  <td><a href="">Baju Branding</a></td><td><small>Teknologi & Komunikasi</small></td><td><small>Handphone & Tablet</small></td><td>Aktif</td>
                  <td><button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">Ubah</button> <button type="button" class="btn btn-warning btn-xs">Reset</button>
                      </td>
                </tr><tr role="row" class="odd">
                  <td class="sorting_1">154</td>
                  <td><a href="">Baju Branding</a></td><td><small>Teknologi & Komunikasi</small></td><td><small>Handphone & Tablet</small></td><td>Aktif</td>
                  <td><button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">Ubah</button> <button type="button" class="btn btn-warning btn-xs">Reset</button>
                      </td>
                </tr><tr role="row" class="even">
                  <td class="sorting_1">154</td>

                  <td><a href="">Baju Branding</a></td><td><small>Teknologi & Komunikasi</small></td><td><small>Handphone & Tablet</small></td><td>Aktif</td>
                  <td><button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">Ubah</button> <button type="button" class="btn btn-warning btn-xs">Reset</button>
                      </td>
                </tr><tr role="row" class="odd">
                  <td class="sorting_1">154</td>
                  <td><a href="">Baju Branding</a></td><td><small>Teknologi & Komunikasi</small></td><td><small>Handphone & Tablet</small></td><td>Aktif</td>
                  <td><button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">Ubah</button> <button type="button" class="btn btn-warning btn-xs">Reset</button>
                      </td>
                </tr><tr role="row" class="even">
                  <td class="sorting_1">154</td>

                  <td><a href="">Baju Branding</a></td><td><small>Teknologi & Komunikasi</small></td><td><small>Handphone & Tablet</small></td><td>Aktif</td>
                  <td><button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">Ubah</button> <button type="button" class="btn btn-warning btn-xs">Reset</button>
                      </td>
                </tr><tr role="row" class="odd">
                  <td class="sorting_1">154</td>
                  <td><a href="">Baju Branding</a></td><td><small>Teknologi & Komunikasi</small></td><td><small>Handphone & Tablet</small></td><td>Aktif</td>
                  <td><button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">Ubah</button> <button type="button" class="btn btn-warning btn-xs">Reset</button>
                      </td>
                </tr><tr role="row" class="even">
                  <td class="sorting_1">154</td>

                  <td><a href="">Baju Branding</a></td><td><small>Teknologi & Komunikasi</small></td><td><small>Handphone & Tablet</small></td><td>Aktif</td>
                  <td><button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">Ubah</button> <button type="button" class="btn btn-warning btn-xs">Reset</button>
                      </td>
                </tr><tr role="row" class="odd">
                  <td class="sorting_1">154</td>
                  <td><a href="">Baju Branding</a></td><td><small>Teknologi & Komunikasi</small></td><td><small>Handphone & Tablet</small></td><td>Aktif</td>
                  <td><button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">Ubah</button> <button type="button" class="btn btn-warning btn-xs">Reset</button>
                      </td>
                </tr><tr role="row" class="even">
                  <td class="sorting_1">154</td>

                  <td><a href="">Baju Branding</a></td><td><small>Teknologi & Komunikasi</small></td><td><small>Handphone & Tablet</small></td><td>Aktif</td>
                  <td><button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">Ubah</button> <button type="button" class="btn btn-warning btn-xs">Reset</button>
                      </td>
                </tr></tbody>
                <tfoot>
                <tr><th rowspan="1" >ID</th><th rowspan="1" >Title Product</th><th rowspan="1" >Current Category</th><th rowspan="1" >New Category</th><th rowspan="1" >Status</th><th rowspan="1" >Action</th><th rowspan="1" > </th></tr>
                </tfoot>
              </table></div></div>

              <div class="col-md-12 text-center">
                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal2">Submit</button>
              </div>

            </div></div>
            <!-- /.box-body -->
          </div>
            
            </div><!-- /.col (right) -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>



     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
