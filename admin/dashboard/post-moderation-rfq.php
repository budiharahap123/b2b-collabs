<?php include("header.html"); ?>

<body class="skin-red-light sidebar-mini">
	<div class="wrapper">
		<?php include("top-navigation.html"); ?>
		<?php include("leftside.html"); ?>
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
		        <section class="content-header">
		          <h1>
		            Permintaan/RFQ
		          </h1>
		        </section>
		        <!-- Main content -->
		        <section class="content">
		        	<div class="row">
			            <div class="col-md-12">
				            <div class="box">
				                <div class="box-header with-border">
				                  <h3 class="box-title">Post Moderation Permintaan</h3>
				                </div><!-- /.box-header -->
				              	<form class="form-horizontal">
					                <div class="box-body">
					                    <div class="form-group">
						                    <label class="col-sm-2">ID Permintaan</label>
						                    <div class="col-sm-9"><input type="text" class="form-control" placeholder="ID Permintaan"></div>
					                    </div>
					                    <div class="form-group">
						                    <label class="col-sm-2">Keyword</label>
						                    <div class="col-sm-9"><input type="text" class="form-control" placeholder="Keyword untuk judul dan deskripsi"></div>
					                    </div>
					                    <div class="form-group">
						                    <label class="col-sm-2">Tanggal Dibuat</label>
						                    <div class="col-sm-9">
						                    	<div class="input-group">
									                <div class="input-group-addon">
									            	    <i class="fa fa-clock-o"></i>
									              	</div>
									                <input type="text" class="form-control pull-right" id="reservationtime">
								                </div><!-- /.input group -->
								            </div>
					                    </div>
					                </div>
				                </form>
					            <div class="box-body no-padding">               
					                <div class="col-sm-12">
					                    <table id="example2" class="table table-bordered table-hover" >
					                    <tbody>
					                        <tr>
						                        <th class="col-md-1">ID</th>
						                        <th class="col-md-2">Tanggal dibuat<i class="fa fa-sort-amount-desc pull-right" aria-hidden="true"></i></th>
						                        <th class="col-md-2">Title Product</th>
						                        <th class="col-md-5">Description</th>
						                        <th class="col-md-1">Category</th>
						                        <th class="col-md-1">Status</th>
						                        <th class="col-md-1">Action</th>
					                        </tr>
					                        <tr>
					                        	<td>123456</td>
						                        <td>25/02/2016</td>
						                        <td>Controller XBOX 360</td>
						                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.</td>
						                        <td>Video Game</td>
						                        <td>Aktif</td>
						                        <td><a href="post-moderation-rfq-detail.php" class="btn btn-flat btn-danger">Detail</a></td>
					                        </tr>
					                        <tr>
					                        	<td>123456</td>
						                        <td>25/02/2016</td>
						                        <td>Controller XBOX 360</td>
						                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.</td>
						                        <td>Video Game</td>
						                        <td>Aktif</td>
						                        <td><a href="post-moderation-rfq-detail.php" class="btn btn-flat btn-danger">Detail</a></td>
					                        </tr>
					                        <tr>
					                        	<td>123456</td>
						                        <td>25/02/2016</td>
						                        <td>Controller XBOX 360</td>
						                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.</td>
						                        <td>Video Game</td>
						                        <td>Aktif</td>
						                        <td><a href="post-moderation-rfq-detail.php" class="btn btn-flat btn-danger">Detail</a></td>
					                        </tr>
					                        <tr>
					                        	<td>123456</td>
						                        <td>25/02/2016</td>
						                        <td>Controller XBOX 360</td>
						                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.</td>
						                        <td>Video Game</td>
						                        <td>Aktif</td>
						                        <td><a href="post-moderation-rfq-detail.php" class="btn btn-flat btn-danger">Detail</a></td>
					                        </tr>
					                        <tr>
					                        	<td>123456</td>
						                        <td>25/02/2016</td>
						                        <td>Controller XBOX 360</td>
						                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.</td>
						                        <td>Video Game</td>
						                        <td>Aktif</td>
						                        <td><a href="post-moderation-rfq-detail.php" class="btn btn-flat btn-danger">Detail</a></td>
					                        </tr>
					                        <tr>
					                        	<td>123456</td>
						                        <td>25/02/2016</td>
						                        <td>Controller XBOX 360</td>
						                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.</td>
						                        <td>Video Game</td>
						                        <td>Aktif</td>
						                        <td><a href="post-moderation-rfq-detail.php" class="btn btn-flat btn-danger">Detail</a></td>
					                        </tr>
					                        <tr>
					                        	<td>123456</td>
						                        <td>25/02/2016</td>
						                        <td>Controller XBOX 360</td>
						                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.</td>
						                        <td>Video Game</td>
						                        <td>Aktif</td>
						                        <td><a href="post-moderation-rfq-detail.php" class="btn btn-flat btn-danger">Detail</a></td>
					                        </tr>
					                        <tr>
					                        	<td>123456</td>
						                        <td>25/02/2016</td>
						                        <td>Controller XBOX 360</td>
						                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.</td>
						                        <td>Video Game</td>
						                        <td>Aktif</td>
						                        <td><a href="post-moderation-rfq-detail.php" class="btn btn-flat btn-danger">Detail</a></td>
					                        </tr>
					                        <tr>
					                        	<td>123456</td>
						                        <td>25/02/2016</td>
						                        <td>Controller XBOX 360</td>
						                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.</td>
						                        <td>Video Game</td>
						                        <td>Aktif</td>
						                        <td><a href="post-moderation-rfq-detail.php" class="btn btn-flat btn-danger">Detail</a></td>
					                        </tr>
					                        <tr>
					                        	<td>123456</td>
						                        <td>25/02/2016</td>
						                        <td>Controller XBOX 360</td>
						                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel dui leo. Vivamus cursus mollis elit, vitae ornare mi. Ut dapibus, urna eget commodo tincidunt, purus risus lobortis ipsum, sed ultricies velit diam at urna.</td>
						                        <td>Video Game</td>
						                        <td>Aktif</td>
						                        <td><a href="post-moderation-rfq-detail.php" class="btn btn-flat btn-danger">Detail</a></td>
					                        </tr>
					                    </tbody>
					                    </table>
					                    <div class="row">
						                  <div class="col-sm-5 pull-right">
							                  <div class="dataTables_paginate paging_simple_numbers pull-left" id="example1_paginate"><ul class="pagination"><li class="paginate_button next" id="example1_next"><a href="#">First</a></li><li class="paginate_button previous disabled" id="example1_previous"><a href="#">Previous</a></li><li class="paginate_button next" id="example1_next"><a href="#">Next</a></li><li class="paginate_button next" id="example1_next"><a href="#">Last</a></li></ul>
							                  </div>
							                  <span class="col-sm-4 pagination dataTables_info pull-right"> 1 / 123456 pages</span>
						                  </div>
					                    </div>
					                </div>
					            </div><!-- /.box-body -->
				          	</div>
			            </div><!-- /.col (right) -->
			        </div>
		        </section>
			</div>
		<?php include("footer.html"); ?>
		<?php include("rightside.html"); ?>
	<!-- Page script -->
         <script>
		    $(function () {
		      $('#example2').DataTable({
		        "paging": true,
		        "lengthChange": false,
		        "searching": false,
		        "ordering": true,
		        "info": true,
		        "autoWidth": false
		      });
		    });
		   </script>
	</div>
</body>