
<?php include("header.html"); ?>

  <body class="skin-red-light sidebar-mini">
    <div class="wrapper">

<?php include("top-navigation.html"); ?>
      
<?php include("leftside.html"); ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Verification Print
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">


          <div class="row">

            <div class="col-md-8">

              <div class="box box-primary invoice">

                <div class="margin">

                <div class="box-header">
                  <img src="../../assets/images/logo.png">
                </div>
                <div class="box-body">
                  <p>Kepada Yth.</p>                    
                  <small class="pull-right">Date: 2/10/2014</small>
                  <p class="lead">{User.name},</p>

                  <p>
                    Berdasarkan Permintaan Anda,<br />
                    Berikut adalah kode validasi yang harus anda masukkan ke dalam sistem kami melalui link<br />
                    www.idn.co.id/nama.link :
                  </p>
                  <h2 class="well text-center no-shadow code">
                    <span>A47UX</span><span>CSV15</span><span>17ABC</span><span>D3FUC</span>
                  </h2>
                  <p>
                    Kode tersebut akan expired pada tanggal
                  </p>
                  <p class="lead">
                    dd/mm/yyyy.
                  </p>
                  <p>
                    Segera masukkan kode validasi tersebut diatas untuk verifikasi alamat anda.
                  </p>


                  <div class="printbutton text-center no-print">
                    <a href="invoice-print.html" target="_blank" class="btn btn-primary btn-default"><i class="fa fa-print"></i> Print</a>
                  </div>

                </div>

              </div>


              </div>

            </div><!-- /.col (right) -->

          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<?php include("footer.html"); ?>

<?php include("rightside.html"); ?>

<?php include("general-script.html"); ?>



     
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


  </body>
</html>
